<?php
include("../dbcon.php");
$s_id = date("YmdHis").$_POST["counter"];
$a_id = date("YmdHis").$_POST["counter"];
$e_id = date("YmdHis").$_POST["counter"];
$f_id = date("YmdHis").$_POST["counter"];
$r_id = date("YmdHis").$_POST["counter"];
if (is_numeric($r_id) == true && is_numeric($f_id) == true && is_numeric($e_id) == true && is_numeric($a_id) == true && is_numeric($s_id) == true){
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
      $payment = $_POST["payment"]; 
      if(isset($_POST["type"])){
          $type = $_POST["type"]; 
      };
      if(!isset($_POST["type"])){
          $type = ""; 
      };
      $employee = $_POST["employee"]; 
      $project = $_POST["project"];
      $lname = $_POST["lname"];
      $fname = $_POST["fname"];
      $mname = $_POST["mname"];
      $msurname = $_POST["msurname"];
      $address = $_POST["address"];
      $email = $_POST["email"];
      $dbirth = $_POST["dbirth"];
      $dplace = $_POST["dplace"];
      $status = $_POST["status"];
      $sex = $_POST["sex"];
      $citizen = $_POST["citizen"];
      $contact = $_POST["contact"];
      $office = $_POST["office"];
      $tin = $_POST["tin"]; 
      $pagibig = $_POST["pagibig"];
      $s_lname = $_POST["s_lname"];
      $s_fname = $_POST["s_fname"];
      $s_mname = $_POST["s_mname"];
      $s_msurname = $_POST["s_msurname"];
      $s_dbirth = $_POST["s_dbirth"];
      $s_dplace = $_POST["s_dplace"];
      $nature = $_POST["nature"];
      $agency = $_POST["agency"];
      $a_address = $_POST["a_address"];
      $famIncome = $_POST["famIncome"];
      $noViolation = $_POST["noViolation"];
      $altViolation = $_POST["altViolation"];
      $SG = $_POST["income"];
      $date = date("Y-m-d");
      $app_date = date("Y-m-d");
      $s_final = "$s_id";
      $a_final = "$a_id";
      $e_final = "$e_id";
      $f_final = "$f_id";
      $r_final = "$r_id";



      $target_dir = "gehpbackend/pages/forms/uploads/OEC_uploads/";
      $target_filevideo = $target_dir . basename("$project-$lname-$s_id.pdf");
      $uploadOkvideo = 1;
      $video = "$project-$lname-$s_id.pdf";
      $imageFileTypevideo = strtolower(pathinfo($target_filevideo,PATHINFO_EXTENSION));
      
      
        //unit_image6
        // Check if image file is a actual image or fake image
        //if(isset($_POST["submit"])) {
          
          //$checkvideo = getimagesize($_FILES["myFile"]["tmp_name"]);
          //if($checkvideo !== false) {
            //echo "File is valid.";
            //$uploadOkvideo = 1;
          //} else {
            //echo "File is not valid.";
            //$uploadOkvideo = 0;
          //}
        //}
      
        // Check if file already exists
        //if (file_exists($target_file)) {
        //  echo "Sorry, file already exists.";
        //  $uploadOk = 0;
        //}
      
        // Check file size
        if ($_FILES["myFile"]["size"] > 20000000) {
          $uploadOkvideo = 0;
        }
      
        // Allow certain file formats
        if($imageFileTypevideo != "pdf") {
          $uploadOkvideo = 0;
        }
      
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOkvideo == 0) {
          echo "<script>
                alert('Sorry, something went wrong and your application was not submitted.');
                window.location.href='application-form-ofw.php';
                </script>";
        // if everything is ok, try to upload file
        } 
        else
        
        {
      
        if ( is_numeric($project) == true){
          $datasearch=[
                  'project' => $project, 'lname' => $lname, 'fname' => $fname, 'mname' => $mname, 'dbirth' => $dbirth,
          ];
          $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $querysearch = "SELECT PJ_CODE FROM ofw_applications 
                        LEFT JOIN ofw_applicant_info ON ofw_applications.applicant_id = ofw_applicant_info.applicant_id
                        WHERE PJ_CODE = :project AND LNAME = :lname AND FNAME = :fname AND MNAME = :mname AND BDATE = :dbirth";
          $sthcount= $dbh->prepare($querysearch);
          $sthcount->execute($datasearch);
          $app_count=$sthcount->rowCount();
          if($app_count>0) {
                  
                  echo "<script>
                  alert('Sorry, this application is still on process.');
                  window.location.href='application-form-ofw.php';
                  </script>";
          }
          else {
      
            $sql_applicant_infodata = [
              ':a_final' => $a_final,
              ':lname' => $lname,
              ':fname' => $fname,
              ':mname' => $mname,
              ':msurname' => $msurname,
              ':address' => $address,
              ':email' => $email,
              ':dbirth' => $dbirth,
              ':dplace' => $dplace,
              ':status' => $status,
              ':sex' => $sex,
              ':citizen' => $citizen,
              ':contact' => $contact,
              ':office' => $office,
              ':tin' => $tin,
              ':pagibig' => $pagibig,
              ':famIncome' => $famIncome,
              ':noViolation' => $noViolation,
              ':altViolation' => $altViolation
            ];
            $sql_applicant_info = "INSERT INTO ofw_applicant_info (applicant_id, LNAME, FNAME, MNAME, motherSurname, ADDR1, email, BDATE, birthplace, CSTATUS, sex, citizenship, contact, office, tin, pagibig, fam_income, non_violation, alt_violation) 
            VALUES 
            (:a_final, :lname, :fname, :mname, :msurname, :address, :email, :dbirth, :dplace, :status, :sex, :citizen, :contact, :office, :tin, :pagibig, :famIncome, :noViolation, :altViolation)";
            $sthsql_applicant_info = $dbh->prepare($sql_applicant_info);
            
            $sql_applicant_spousedata = [
              ':s_final' => $s_final,
              ':s_lname' => $s_lname,
              ':s_fname' => $s_fname,
              ':s_mname' => $s_mname,
              ':s_msurname' => $s_msurname,
              ':s_dbirth' => $s_dbirth,
              ':s_dplace' => $s_dplace
            ];
            $sql_applicant_spouse = "INSERT INTO ofw_applicant_spouse (spouse_id, SLNAME, SFNAME, SMNAME, SmotherSurname, SBDATE, Sbirthplace) 
            VALUES 
            (:s_final, :s_lname, :s_fname, :s_mname, :s_msurname, :s_dbirth, :s_dplace)";
            $sthsql_applicant_spouse = $dbh->prepare($sql_applicant_spouse);
            
            $sql_employment_statusdata = [
              ':e_final' => $e_final,
              ':nature' => $nature,
              ':agency' => $agency,
              ':a_address' => $a_address,
              ':SG' => $SG
            ];
            $sql_employment_status = "INSERT INTO ofw_employment_status (employment_id, nature, EMPLOYER, emp_address, MINCOME) 
            VALUES 
            (:e_final, :nature, :agency, :a_address, :SG)";
            $sthsql_employment_status = $dbh->prepare($sql_employment_status);

            $stat = "New Applicant";
            $source = "Website/Frontend";
            $sql_applicationsdata = [
              ':payment' => $payment,
              ':type' => $type,
              ':employee' => $employee,
              ':app_date' => $app_date,
              ':project' => $project,
              ':a_final' => $a_final,
              ':s_final' => $s_final,
              ':e_final' => $e_final,
              ':f_final' => $f_final,
              ':r_final' => $r_final,
              ':stat' => $stat,
              ':video' => $video,
              ':source' => $source
            ];
            $sql_applications = "INSERT INTO ofw_applications (payment, house_type, employee, app_date, PJ_CODE, applicant_id, spouse_id, employment_id, dependent_id, req_id, app_status, fileUpload, source) 
            VALUES (:payment, :type, :employee, :app_date, :project, :a_final, :s_final, :e_final, :f_final, :r_final, :stat, :video, :source)";
            $sthsql_applications = $dbh->prepare($sql_applications);
            
            $sql_requirementsdata = [
              ':r_final' => $r_final
            ];
            $sql_requirements = "INSERT INTO requirements_ofw (req_id) VALUES (:r_final)"; 
            $sthsql_requirements = $dbh->prepare($sql_requirements);

              if (move_uploaded_file($_FILES["myFile"]["tmp_name"], $target_filevideo) && $sthsql_applicant_info->execute($sql_applicant_infodata) && $sthsql_applicant_spouse->execute($sql_applicant_spousedata) && $sthsql_employment_status->execute($sql_employment_statusdata) && $sthsql_applications->execute($sql_applicationsdata) && $sthsql_requirements->execute($sql_requirementsdata)) {
                
                for($i=0;$i<count($_POST['dlname']);$i++){
        
                  $DLNAME = $_POST['dlname'][$i];
                  $DFNAME = $_POST['dfname'][$i];
                  $DMNAME = $_POST['dmname'][$i];
                  $relation = $_POST['relation'][$i];
                  $DCSTATUS = $_POST['dcstatus'][$i];
                  $age = $_POST['age'][$i];
                  $source_income = $_POST['source_income'][$i];
                      
                  $sql_familydata = [
                          ':f_final' => $f_final,
                          ':DLNAME' => $DLNAME,
                          ':DFNAME' => $DFNAME,
                          ':DMNAME' => $DMNAME,
                          ':relation' => $relation,
                          ':DCSTATUS' => $DCSTATUS,
                          ':age' => $age,
                          ':source_income' => $source_income
                  ];

                  if($DLNAME!="" && $DFNAME!="" && $DMNAME!="" && $relation!="" && $DCSTATUS!="" && $age!=""){
                    $sql_family = "INSERT INTO ofw_family (dependent_id, DLNAME, DFNAME, DMNAME, relation, DCSTATUS, age, source_income)
                    VALUES
                    (:f_final, :DLNAME, :DFNAME, :DMNAME, :relation, :DCSTATUS, :age, :source_income)";
                    $sthsql_family = $dbh->prepare($sql_family);
                    $sthsql_family->execute($sql_familydata);
                  }
                  else {

                  }
                  
                }
                date_default_timezone_set("Asia/Hong_Kong");  
                $time = date("h:i a");
                $date = date('F d, Y', strtotime(date("Y-m-d")));
                $datetime = $date." ".$time;

                $auditdata = [
                  ':activity' => "Added an Application (OFW_Website/Frontend)",
                  ':username' => 'Applicant',
                  ':datetime' => $datetime
                ];
                $audit = "INSERT INTO audit_trail_applications (activity, username, date) VALUES (:activity, :username, :datetime)";
                $sthaudit = $dbh->prepare($audit);
                $sthaudit->execute($auditdata);
                
                $countAT = "SELECT COUNT(*) as CountTodel FROM audit_trail_applications";
                $sthcountAT = $dbh->prepare($countAT);
                $sthcountAT->execute();
                $sthcountAT->setFetchMode(PDO::FETCH_ASSOC);
                while ($rowCountAT = $sthcountAT->fetch(PDO::FETCH_ASSOC))  {   
                        $CountATres = $rowCountAT['CountTodel']; 
                }   
                if($CountATres > 1000){
                        $delList = $CountATres - 1000;
                        $reduceAT = "DELETE FROM audit_trail_applications ORDER BY id ASC LIMIT ".$delList."";
                        $sthreduceAT = $dbh->prepare($reduceAT);
                        $sthreduceAT->execute();
                }
                if($CountATres <= 1000){

                }
                
                //send email notification
                $app_pass = "pnlvwkmioeeexaqd";
                $email_pass = "nh@_g3hp&";
                $email_sender = "gehp.nha@gmail.com";
                $subjectmail = "Received Application";
                $messagemail = "<p>Good day!</p>
                        <p>We have received your application to NHA <b>".$project."</b> Housing Project.</p>
                        <p>Kindly wait for a separate email from us which will ask you to submit the following documents:</p>
                        
                        <li>Certificate of Compensation</li>
                        <li>BIR Certified Income Tax Return</li>
                        <li>Certificate of Active in Service</li>
                        <li>Birth Certificate (Single)</li>
                        <li>Marriage Certificate</li>
                        <li>Affidavit of Separation-in-Fact</li>
                        <li>Two (2) Government-Issued ID</li>
                        <li>Latest Income Tax Return (ITR)</li>
                        <li>PAG-IBIG Housing Loan Application</li>
                        <li>One (1) Month Pay Slip</li>
                        <li>One (1) Valid ID with Signature</li>
                        <li>Notarized Borrower's Conformity</li>
                        <li>Authority to Deduct Loan Amortization</li>
                        <li>Notarized Affidavit of Two Disinterested Person</li>

                        <p>For further inquiries or follow-ups, you may reach us at <b>02-8922-2490</b> or <b>afppnp.npc@nha.gov.ph</b>.</p>
                        <p>Thank you for your interest in the NHA Overseas Filipino Workers"."'"." Housing Project.</p>";

                require_once ("phpmailer/class.phpmailer.php");
                $Correo = new PHPMailer();
                $Correo->IsSMTP();
                $Correo->SMTPAuth = true;
                $Correo->SMTPSecure = "tls";
                $Correo->Host = "smtp.gmail.com";
                $Correo->Port = 587;
                $Correo->Username = "$email_sender";
                $Correo->Password = "$app_pass";
                $Correo->SetFrom("$email_sender","De Yo");
                $Correo->FromName = "GEHP System Support";
                $Correo->AddAddress("mj.garza@nha.gov.ph");
                $Correo->Subject = "$subjectmail";
                $Correo->Body = "$messagemail";
                $Correo->addReplyTo('afppnp.npc@nha.gov.ph');
                $Correo->IsHTML (true);
                $Correo->Send();
                //end email notification
                
                echo "<script>
                    alert('Your application was submitted successfully.');
                    window.location.href='application-form-ofw.php';
                    </script>";
                    $dbh = null;
              }
              else {
                
                echo "<script>
                alert('Sorry, something went wrong and your application was not submitted.');
                window.location.href='application-form-ofw.php';
                </script>";
                $dbh = null;
              }
          }
        }
        else{
        http_response_code(400);
        die('Error processing bad or malformed request');
        } 
      }
    }
    catch(PDOException $e){
      error_log('PDOException - ' . $e->getMessage(), 0);
      http_response_code(500);
      die('Error establishing connection with database');
    }
  } 
  else{
  http_response_code(400);
  die('Error processing bad or malformed request');
  } 
?>
