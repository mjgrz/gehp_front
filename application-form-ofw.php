<?php include ('assets/pages/header.php') ?>    
<script src="https://www.google.com/recaptcha/api.js"></script>  
<script type="text/javascript" src="http://code.jquery.com/jquery-compat-git.js"></script>                              
<!-- Start Contact -->
    <section class="container py-5">

        <h1 class="col-12 col-xl-8 h2 text-left text-primary pt-3">Application Form</h1>
        <h5 class="col-12 col-xl-8 text-left regular-400 pb-3 typo-space-line">
            For Government Employees/Uniformed Personnel
        </h5>
        <h6 class="col-12 col-xl-6 light-300 mb-5">Application to Purchase a House and Lot Package/Housing Unit Under the Tahanan ng Bagong Bayani Housing Program</h6>
        <!-- End Text Header -->

        <div class="row pb-4">
            <div class="col-lg-4">

                <div class="contact row mb-4 justify">
                    <div class="contact-icon col-lg-3 col-3">
                        <div class="py-3 mb-2 text-center border rounded text-secondary">
                            <i class='display-6 bx bxs-bell'></i>
                        </div>
                    </div>
                    <ul class="reminder-info list-unstyled col-lg-9 col-9">
                        <li class="h5 mb-0 mb-3">Reminder:</li>

                        <p>
                        <span class="light-300">
                           <i class="bx bxs-check-circle me-1"></i>Please read and follow the <b> Steps and Instructions</b>, as well as prepare all of the required documents listed on the <b> GEHP Requirements</b>. 
                        </span>
                        </p>

                        <p>
                        <span class="light-300">
                           <i class="bx bxs-check-circle me-1"></i>To avoid data entry errors, please ensure that all information you provide is complete and honest when filling out the form.
                        </span>
                        </p>

                        <p>
                        <span class="light-300">
                           <i class="bx bxs-check-circle me-1"></i>Kindly enter <b>'N/A'</b> if you don't have an answer to a specific field/s.
                        </span>
                        </p>

                        <p>
                        <span class="light-300">
                           Thank you!
                        </span>
                        </p>
                    </ul>
                </div>

                <div class="contact row mb-4 justify">
                    <div class="contact-icon col-lg-3 col-3">
                        <div class="py-3 mb-2 text-center border rounded text-secondary">
                            <i class='display-6 bx bxs-file'></i>
                        </div>
                    </div>
                    <ul class="reminder-info list-unstyled col-lg-9 col-9">
                        <li class="h5 mb-0 mb-3">Download Application Form</li>
                        <p>
                        <span class="light-300 justify">
                           <i class="bx bxs-check-circle me-1"></i> Download the PDF copy of GEHP Application Form and this shall be accomplished/filed by walk-in applicants. 
                        </span>
                        </p>
                        <p>
                        <a id="downloadpdf" href="print_AppOFW.php">Click here to download</a>
                        <style>
                            #downloadpdf:hover {
                                color: #67A2B2;
                            }
                        </style>
                        </p>
                    </ul>
                </div>

                
            </div>


            <!-- Start OFW Form -->
            <div class="col-lg-8 ">
                <form class="contact-form row" action="submitOFW.php" role="form" id="ContactForm" method="POST" enctype="multipart/form-data">
                    <?php 
                    include("../dbcon.php");
                    try{
                        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $query = "SELECT * FROM ofw_applications";
                        $sthquery = $dbh->prepare($query);
                        $sthquery->execute();
                        $count = $sthquery->rowCount() + 535;
                        $dbh = "";
                    }
                    catch(PDOException $e){
                        error_log('PDOException - ' . $e->getMessage(), 0);
                        http_response_code(500);
                        die('Error establishing connection with database');
                    }
                      ?>
                    <input type="text" name="counter" value="<?php echo $count ?>" hidden>

                    <p class="h5 mb-4 typo-space-line"><b>A.1 FOR PURCHASE OF HOUSE AND LOT PACKAGE </b> <span class="text-muted"> under the GEHP thru</span></p>
                    <!-- Start Cash Sale  -->
                    <div class="col-lg-3 row mb-3">
                        <label class="container">
                          <input type="radio" name="payment" value="Cash Sale" required>
                          <span class="checkmark me-2"></span>
                          Cash Sale 
                        </label>
                    </div>
                    <!-- End Cash Sale  -->

                    <!-- Start Staggered Cash -->
                    <div class="col-lg-3 row mb-3">
                        <label class="container">
                          <input type="radio" name="payment" value="Staggered Cash" required>
                          <span class="checkmark me-2"></span>
                          Staggered Cash 
                        </label>
                    </div>
                    <!-- End Staggered Cash -->

                    <!-- Start End-User Financing -->
                    <div class="col-lg-5 row mb-3">
                        <label class="container">
                          <input type="radio" name="payment" value="End-User Financing (MTO-Pag-IBIG)" required>
                          <span class="checkmark me-2"></span>
                          End-User Financing (MTO-Pag-IBIG)
                        </label>
                    </div>
                    <!-- End End-User Financing -->

                    <div class="col-lg-12 row">
                        <div class="col-lg-5 row mb-3" style="width: auto;">
                            <label><b>In-House Financing (Installment Payment):</b></label>
                        </div> 
                        <!-- Start In-House Financing -->
                        <div class="col-lg-4 row mb-3" style="width: auto;">
                            <label class="container">
                            <input type="radio" name="payment" value="Straight Amortization" required>
                            <span class="checkmark me-2"></span>
                            Straight Amortization
                            </label>
                        </div>
                        <!-- In-House Financing -->
                        
                        <!-- Start In-House Financing -->
                        <div class="col-lg-4 row mb-3" style="width: auto;">
                            <label class="container">
                            <input type="radio" name="payment" value="Escalating Amortization" required>
                            <span class="checkmark me-2"></span>
                            Escalating Amortization
                            </label>
                        </div>
                        <!-- In-House Financing -->
                    </div>

                    <!-- Start Straight Amortization>
                    <div class="col-lg-4 row mb-3">
                        <label class="container">
                          <input type="radio" name="payment" value="Straight Amortization" required>
                          <span class="checkmark me-2"></span>
                          Straight Amortization 
                        </label>
                    </div>
                    < Straight Amortization -->

                    <!-- Start Escalating Amortization >
                    <div class="col-lg-4 row mb-3">
                        <label class="container">
                          <input type="radio" name="payment" value="Escalating Amortization" required>
                          <span class="checkmark me-2"></span>
                          Escalating Amortization 
                        </label>
                    </div>
                    < Escalating Amortization  -->

                    <div class="col-lg-12 mb-3">
                        <div class="form-floating">
                            <select type="select" class="form-control btn-outline-primary bg-transparent text-dark" id="project" onchange="getProject()"
                                name="project" placeholder="Housing Project" required style="font-size: 12px;">
                            <option value="" disabled selected>Project Name</option>
                            <?php
                                include("../dbcon.php");
                                try{
                                    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                                    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                    $regionselect = "SELECT  project_info.PJ_CODE, region.Region, project_info.PJ_NAME, units_floor.location FROM project_info 
                                                    LEFT JOIN units_floor ON project_info.project_id = units_floor.project_id
                                                    LEFT JOIN region ON project_info.region = region.psgc_reg
                                                    WHERE archived != '1' AND PJ_NAME != 'SCOUT RANGER VILLE' AND availability = 'Available' AND project_status != 'Upcoming'";
                                    $sthregionselect = $dbh->prepare($regionselect);
                                    $sthregionselect->execute();
                                    $sthregionselect->setFetchMode(PDO::FETCH_ASSOC); 
                                    if($sthregionselect->rowCount() > 0){
                                        while ($regionrow = $sthregionselect->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                    <option value="<?php echo $regionrow['PJ_CODE'] ?>"><?php echo $regionrow['Region'] ?> | <?php echo $regionrow['PJ_NAME'] ?> (<?php echo $regionrow['location'] ?>)</option>
                                    <?php 
                                    } 
                                }
                                else { }
                                }
                                catch(PDOException $e){
                                    error_log('PDOException - ' . $e->getMessage(), 0);
                                    http_response_code(500);
                                    die('Error establishing connection with database');
                                }
                                ?>
                            </select>
                            <label for="floatingname light-300">Select</label>
                        </div>
                        <div><span id="project-status"></span></div>
                        <script type="text/javascript">
                            function getProject(val){
                                $("#loaderIcon").show();
                                jQuery.ajax({
                                    url: "ajaxProject.php",
                                    data:'project='+$("#project").val(),
                                    type: "POST",
                                    success:function(data){
                                        $("#project-status").html(data);
                                        $("#loaderIcon").hide();
                                        },
                                    error:function (){
                                    }
                                });
                            }
                        </script>
                    </div><!-- End Select Project Name -->

                    <p class="h5 mb-4 typo-space-line"><b>A.2 TYPE OF HOUSING</b></p>
                    <div class="col-lg-3 row mb-4">
                        <label class="container">
                          <input type="radio" name="type" value="2-Storey Duplex">
                          <span class="checkmark me-2"></span>
                          2 Storey Duplex
                        </label>
                    </div>
                    <!-- End 2 Storey Duplex -->

                    <div class="col-lg-3 row mb-4">
                        <label class="container">
                          <input type="radio" name="type" value="1-Storey Duplex">
                          <span class="checkmark me-2"></span>
                          1 Storey Duplex
                        </label>
                    </div>
                    <!-- End 1 Storey Duplex -->

                    <div class="col-lg-5 row mb-4">
                        <label class="container">
                          <input type="radio" name="type" value="Low-Rise Building/Condominium">
                          <span class="checkmark me-2"></span>
                          Low-Rise Building/Condominium
                        </label>
                    </div>
                    <!-- End Low-Rise Building/Condominium -->

                    <p class="h5 mb-4 typo-space-line"><b>A.3 As Overseas Filipino Worker in</b></p>
                    <div class="col-lg-12 mb-5   ">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="employee" name="employee" required
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Name of Government Agency">
                            <label for="floatingname text-dark regular-400">Name of the country where you work as an OFW</label>
                        </div>
                    </div><!-- End Input Last Name -->



                    <p class="h5 mb-5 typo-space-line"><b>I. APPLICANT'S IDENTITY</b> <span class="h6 text-muted">(For female applicant/spouse, provide a complete maiden name)</span></p>

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="lname" name="lname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Last Name" required>
                            <label for="floatingname light-300">Last Name</label>
                        </div>
                    </div><!-- End Input Last Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="fname" name="fname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="First Name" required>
                            <label for="floatingname light-300">First Name</label>
                        </div>
                    </div><!-- End Input First Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="mname" name="mname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Middle Name" required>
                            <label for="floatingname light-300">Middle Name</label>
                        </div>
                    </div><!-- End Input Middle Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="msurname" name="msurname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Mother's Surname" required>
                            <label for="floatingname light-300">Mother's Surname</label>
                        </div>
                    </div><!-- End Input Mother's Surname -->

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="address" name="address"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Residence Address" required>
                            <label for="floatingemail light-300">Residence Address</label>
                        </div>
                    </div><!-- End Input Residence Address -->
                    
                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="dplace" name="dplace"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Place of Birth" required>
                            <label for="floatingname light-300">Place of Birth</label>
                        </div>
                    </div><!-- End Place of Birth -->

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="email" name="email"
                            placeholder="Residence Address" required>
                            <label for="floatingphone light-300">Email address</label>
                        </div>
                    </div><!-- End Input Email address -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="date" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="dbirth" name="dbirth" placeholder="Date of Birth"
                            min="<?php echo Date('Y-m-d', strtotime('-65 year')); ?>" max="<?php echo date('Y-m-d', strtotime('-18 year')); ?>" required>
                            <label for="floatingname light-300">Date of Birth</label>
                        </div>
                    </div><!-- End Date of Birth -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <select type="select" class="form-control btn-outline-primary bg-transparent text-dark" id="sex" name="sex" placeholder="Select Sex" required>
                              <option value="" disabled selected>Sex</option>
                              <option value="male">Male</option>
                              <option value="female">Female</option>
                            </select>
                            <label for="floatingname light-300">Select</label>
                        </div>
                    </div><!-- End Sex -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <select type="select" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="status" name="status" placeholder="Select Civil Status" required>
                            <option value="" disabled selected>Civil Status</option>
                              <option value="single">Single</option>
                              <option value="married">Married</option>
                              <option value="widowed">Widowed</option>
                              <option value="separated-in-fact">Separated-in-Fact</option>
                              <option value="legally-separated">Legally-Separated</option>
                              <option value="single-HOF">Single-HOF</option>
                            </select>
                            <label for="floatingname light-300">Select</label>
                        </div>
                    </div><!-- End Select Civil Status -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="citizen" name="citizen"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Citizenship" required>
                            <label for="floatingname light-300">Citizenship</label>
                        </div>
                    </div><!-- End Place of Birth -->

                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="contact" name="contact" pattern="[0-9]+"
                            oninput="this.value = this.value.replace(/[^0-9-]/g, '').replace(/(\..*)\./g, '$1');" maxlength="11" placeholder="Contact Number (Residence)" required />
                            <label for="floatingemail light-300">Contact Number (Residence)</label>
                        </div>
                    </div><!-- End Contact Number (Residence) -->

                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="office" name="office" pattern="[0-9]+"
                            oninput="this.value = this.value.replace(/[^0-9-]/g, '').replace(/(\..*)\./g, '$1');" maxlength="11" placeholder="Office"/>
                            <label for="floatingphone light-300">Office</label>
                        </div>
                    </div><!-- End Office -->

                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="tin" name="tin"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="TIN No." required>
                            <label for="floatingphone light-300">TIN No.</label>
                        </div>
                    </div><!-- End TIN -->

                    <div class="col-lg-6 mb-4">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="pagibig" name="pagibig"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Pag-IBIG No." required>
                            <label for="floatingphone light-300">GSIS/SSS/Pag-IBIG Policy No.</label>
                        </div>
                    </div><!-- End Pag-IBIG No. -->

                    <h6><b>Applicant's Spouse / Co-Owner Identity:</b></h6>

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="s_lname" name="s_lname"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Last Name">
                            <label for="floatingname light-300">Last Name</label>
                        </div>
                    </div><!-- End Input Spouse Last Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="s_fname" name="s_fname"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="First Name">
                            <label for="floatingname light-300">First Name</label>
                        </div>
                    </div><!-- End Input Spouse First Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="s_mname" name="s_mname"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Middle Name">
                            <label for="floatingname light-300">Middle Name</label>
                        </div>
                    </div><!-- End Input Spouse Middle Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="s_msurname" name="s_msurname"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Mother's Surname">
                            <label for="floatingname light-300">Mother's Surname</label>
                        </div>
                    </div><!-- End Input Spouse Mother's Surname -->

                    <div class="col-lg-6 mb-5">
                        <div class="form-floating">
                            <input type="date" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="s_dbirth" name="s_dbirth" placeholder="Date of Birth">
                            <label for="floatingname light-300">Date of Birth</label>
                        </div>
                    </div><!-- End Spouse Date of Birth -->

                    <div class="col-lg-6 mb-5">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="s_dplace" name="s_dplace"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Place of Birth">
                            <label for="floatingname light-300">Place of Birth</label>
                        </div>
                    </div><!-- End Spouse Place of Birth -->

                    <p class="h5 mb-5 typo-space-line"><b>II. APPLICANT'S EMPLOYMENT STATUS</b></p>
                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="nature" name="nature"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Nature of Employment" required>
                            <label for="floatingname light-300">Nature of Employment</label>
                        </div>
                    </div><!-- End Nature of Employment -->

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="agency" name="agency"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Name of Agency/Corporation" required>
                            <label for="floatingname light-300">Name of Agency/Corporation</label>
                        </div>
                    </div><!-- End Name of Agency/Corporation -->

                    

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400"
                            oninput="this.value = this.value.replace(/[^0-9 ₱$€£¥ .,]/g, '').replace(/(\..*)\./g, '$1');"
                            maxlength="15" id="income" name="income" placeholder="Income" required="">
                            <label for="floatingname light-300">Income</label>
                        </div>
                    </div><!-- End Income -->

                    <div class="col-lg-12 mb-5">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="a_address" name="a_address"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Name of Agency/Corporation" required>
                            <label for="floatingname light-300">Address of Agency/Corporation</label>
                        </div>
                    </div><!-- End Address of Agency/Corporation -->

                    <p class="h5 mb-5 typo-space-line"><b>III. APPLICANT'S FAMILY COMPOSITION</b> <span class="text-muted h6 regular-400">(Add field/s if neccessary)</span></p>

                    <div class="multi-field-wrapper">
                        <div class="multi-fields">
                            <div class="col-lg-12 row multi-field" style="width: auto;">
                                <div class="col-lg-4 mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="sl form-control btn-outline-primary bg-transparent text-dark regular-400" name="slno[]" id="slno"  placeholder="Last Name" hidden readonly=""/>
                                        <input type="text" class="sl form-control btn-outline-primary bg-transparent text-dark regular-400" id="fam-lname" name="dlname[]"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');"  placeholder="Last Name"/>
                                        <label for="floatingname light-300">Last Name</label>
                                    </div>
                                </div><!-- End Input Last Name -->

                                <div class="col-lg-4 mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control btn-outline-primary text-dark bg-transparent regular-400" id="fam-fname" name="dfname[]"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="First Name">
                                        <label for="floatingname text-dark light-300">First Name</label>
                                    </div>
                                </div><!-- End Input First Name -->

                                <div class="col-lg-4 mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="fam-fname" name="dmname[]"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Middle Name">
                                        <label for="floatingname light-300">Middle Name</label>
                                    </div>
                                </div><!-- End Input Middle Name -->

                                <div class="col-lg-3 mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="fam-mname" name="relation[]"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Relation to Applicant">
                                        <label for="floatingname light-300" style="font-size: 15px; line-height: 16px;">Relation to Applicant</label>
                                    </div>
                                </div><!-- End Input Relation to Applicant -->

                                <div class="col-lg-3 mb-3">
                                    <div class="form-floating">
                                        <select type="select" class="form-control btn-outline-primary bg-transparent text-dark" id="dcstatus" name="dcstatus[]" placeholder="Select Civil Status" >
                                            <option value="" disabled selected>Civil Status</option>
                                            <option value="single">Single</option>
                                            <option value="married">Married</option>
                                            <option value="widowed">Widowed</option>
                                            <option value="separated-in-fact">Separated-in-Fact</option>
                                            <option value="legally-separated">Legally-Separated</option>
                                            <option value="single-HOF">Single-HOF</option>
                                        </select>
                                        <label for="floatingname light-300">Select</label>
                                    </div>
                                </div><!-- End Input Civil Status -->

                                <div class="col-lg-3 mb-3">
                                    <div class="form-floating">
                                        <input type="number" class="form-control btn-outline-primary bg-transparent text-dark regular-400" min="0" max="150" id="fam-age" name="age[]" placeholder="Age">
                                        <label for="floatingname light-300">Age</label>
                                    </div>
                                </div><!-- End Input Age -->

                                <div class="col-lg-3 mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="fam-age" name="source_income[]"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Age">
                                        <label for="floatingname light-300" style="font-size: 15px;">Source of Income</label>
                                    </div>
                                </div><!-- End Input Source of Income -->
                                <div class="col-lg-12 mb-3"> <hr> </div>
                            </div>
                        </div>
                        <div id="next"></div>
                        <button type="button" id="addrow" class="add-field btn btn-outline-primary btn-primary text-light regular-400 shadow-none mb-5"><i class='bx bx-plus'></i> Add member </button>
                    </div>


                    <p style="float: left;" class="h5 mb-5 typo-space-line"><b>IV. APPLICANT'S TOTAL FAMILY INCOME PER MONTH:</b></p>
                    <div class="col-lg-6 mb-5">
                        <div class="form-floating">
                            <input type="text" pattern="[0-9]+" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" maxlength="15" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="famIncome" name="famIncome" placeholder="famIncome" required>
                            <label style="font-weight: normal;" for="floatingname light-300">Total Family Income</label>
                        </div>
                    </div>

                    <p style="float: left;" class="h5 mb-4 typo-space-line">
                    <b>V. FAMILY REAL PROPERTY HOLDINGS:</b> 
                    
                    </p>
                    <span class="text-muted h6 regular-400">Have never availed of any form of government housing assistance</span>
                    <div class="col-lg-2 mb-5">
                        <label class="container">
                          <input type="radio" name="noViolation" value="YES" required>
                          <span class="checkmark me-2"></span>
                          YES
                        </label>
                    </div>
                    <div class="col-lg-2 mb-5">
                        <label class="container">
                          <input type="radio" name="noViolation" value="NO" required>
                          <span class="checkmark me-2"></span>
                          NO
                        </label>
                    </div>
                    <span class="text-muted h6 regular-400">Have sold, alienated, conveyed, encumbered or leased the socialized housing, including imporovements
                              or rights thereon, except to qualified beneficiary as determined by the Government Agency</span>
                    <div class="col-lg-2 mb-5">
                        <label class="container">
                          <input type="radio" name="altViolation" value="YES" required>
                          <span class="checkmark me-2"></span>
                          YES
                        </label>
                    </div>
                    <div class="col-lg-2 mb-5">
                        <label class="container">
                          <input type="radio" name="altViolation" value="NO" required>
                          <span class="checkmark me-2"></span>
                          NO
                        </label>
                    </div>


                    <label class="h5 pb-4 typo-space-line">VI. ATTACH OEC FILE <span class=" h6 text-muted">(Overseas Employment Certificate)</span></label>
                    <div class="col-lg-12 row">
                        <div class="mb-3">
                            <label>Read the following:</label>
                            <ul>
                                <li>Submit a scanned copy of your OEC if you are a current or former OFW.</li>
                                <li>Submit a scanned copy of OEC of the OFW if you are an OFW's dependent.</li>
				<li>Please submit your OEC copy in PDF file format.</li>
                            </ul>
                        </div>
                        <div class="mb-5">
                             <input type="file" id="myFile" name="myFile" requied>
                        </div>
                    </div>
                    <!-- End Submit File -->

                    <div class="mb-3">
                    <p class="text-primary regular-400">
                        <i class='bx bx-shield-quarter'></i>
                        By clicking Submit, you agree to our <a href="terms-of-service.php" class="text-secondary">Terms of Service</a> and that you have read our <a href="privacy-policy.php" class="text-secondary">Privacy Policy</a>.
                    </p>
                    </div><!-- End Privacy Policy -->

                    <div class="mb-5" style="float: right;">
                        <div class="g-recaptcha brochure__form__captcha" data-sitekey="6LdtNr8bAAAAAPx-lyX2nnrC-eri0pgtA357HQfA"></div>
                    </div><!-- End reCAPTCHA -->

                    <div class="col-md-12 col-12 m-auto text-end">
                        <button type="submit" id="submit_btn" class="btn btn-outline-primary bg-primary text-light rounded-pill px-md-5 radius-0 text-light regular-400" name="submit" value="submit">Submit</button>
                    </div>
                </form>
            </div>
            <!-- End Application Form -->
        </div>
    </section>
    <!-- End Application Form -->

<?php include ('assets/pages/footer.php') ?>

<style>
        .input{
            border:1px solid #002d32;
        }
        .input:disabled{
            background-color:#e9ecef;
        }
        .input:hover{
            border-color: #67a2b2;   
        }
        .input:focus{
            -webkit-box-shadow: 0 0 0 0.25rem rgba(103, 162, 178, 1);
                    box-shadow: none;
        }
    </style>
    <script>
        function disableName() {
            if(document.getElementById("chck").checked){
                document.getElementById("BoS").disabled = false;
                document.getElementById("rank").disabled = false;
                document.getElementById("serial").disabled = false;
                document.getElementById("assignPlace").disabled = false;
                document.getElementById("motherUnit").disabled = false;
            }
            else{
                document.getElementById("BoS").disabled = true;
                document.getElementById("rank").disabled = true;
                document.getElementById("serial").disabled = true;
                document.getElementById("assignPlace").disabled = true;
                document.getElementById("motherUnit").disabled = true;
                document.getElementById("BoS").value = "";
                document.getElementById("rank").value = "";
                document.getElementById("serial").value = "";
                document.getElementById("assignPlace").value = "";
                document.getElementById("motherUnit").value = "";
            }
        }
    </script>