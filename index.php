<?php include ('assets/pages/header.php') ?>
<?php include ('assets/pages/banner/banner.php') ?>


<?php
//Posted Project
try{
    $dbhposted = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbhposted->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $postedselect = "SELECT * FROM posted_project";
    $sthposted = $dbhposted->prepare($postedselect);
    $sthposted->execute();
    $sthposted->setFetchMode(PDO::FETCH_ASSOC);
    while ($postedrow = $sthposted->fetch(PDO::FETCH_ASSOC))  { 
        $title = $postedrow["post_title"];
        $project = $postedrow["project_name"];
        $post_desc = $postedrow["post_desc"];
        $post_img1 = $postedrow["post_image1"];
        $post_img2 = $postedrow["post_image2"];
        $post_img3 = $postedrow["post_image3"];
        $post_img4 = $postedrow["post_image4"];
        $post_img5 = $postedrow["post_image5"];
    }
    $dbhposted = null;
}
catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
}
?>


<!-- Start Ongoing Project -->
    <section class="service-wrapper">
    <div class="container py-5">
        <div class="row">
            <div class="worksingle-content col-lg-8 m-auto text-left justify-content-center">
                <h2 class="h2 text-center col-12 py-5 semi-bold"><?php echo $title ?></h2>
                <h2 class="worksingle-heading h3 pb-3 typo-space-line semi-bold"><i class='bx bx-home h3 mt-1 me-2'></i><?php echo $project ?></h2>
                <p class="worksingle-footer py-3 text-muted regular-400">
                <?php echo $post_desc ?>
                </p>
            </div>
        </div><!-- End Blog Cover -->

        <div class="row justify-content-center pb-4">
            <div class="col-lg-8">
                <div id="templatemo-slide-link-target" class="card mb-3">
                    <a class="" data-type="image" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/postedProject/<?php echo $post_img1 ?>">
                    <img class="img-fluid" src="gehpbackend/pages/forms/uploads/postedProject/<?php echo $post_img1 ?>">
                    </a>
                </div>
                <div class="worksingle-slide-footer row">

                    <!--Start Controls-->
                    <div class="col-1 align-self-center">
                        
                    </div>
                    <!--End Controls-->

                    <!--Start Carousel Wrapper-->
                    <div id="multi-item-example" class="col-12 carousel slide" data-bs-ride="carousel">
                        <!--Start Slides-->
                        <div class="carousel-inner" role="listbox">
                            <!--First slide-->
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col">
                                        <a class="templatemo-slide-link" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/postedProject/<?php echo $post_img2 ?>">
                                            <img class="img-fluid border rounded" src="gehpbackend/pages/forms/uploads/postedProject/<?php echo $post_img2 ?>" alt="Product Image">
                                        </a>
                                    </div>
                                    <div class="col">
                                        <a class="templatemo-slide-link" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/postedProject/<?php echo $post_img3 ?>">
                                            <img class="img-fluid border rounded" src="gehpbackend/pages/forms/uploads/postedProject/<?php echo $post_img3 ?>" alt="Product Image">
                                        </a>
                                    </div>
                                    <div class="col">
                                        <a class="templatemo-slide-link" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/postedProject/<?php echo $post_img4 ?>">
                                            <img class="img-fluid border rounded" src="gehpbackend/pages/forms/uploads/postedProject/<?php echo $post_img4 ?>" alt="Product Image">
                                        </a>
                                    </div>
                                    <div class="col">
                                        <a class="templatemo-slide-link" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/postedProject/<?php echo $post_img5 ?>">
                                            <img class="img-fluid border rounded" src="gehpbackend/pages/forms/uploads/postedProject/<?php echo $post_img5 ?>" alt="Product Image">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/.First slide-->
                        </div>
                        <!--End Slides-->
                    </div>
                </div>
            </div>
        </div><!-- End Slider -->
        </div> 
    </section>
    <!-- End Ongoing Project -->
    
    
    <!-- Start Helping You Build Your Dream banner -->
    <section class="bg-secondary">
        <div class="container py-3">
            <div class="row d-flex justify-content-center text-center">
                <div class="col-lg-3 text-light align-items-center">
                    <img src="assets/img/survey.svg" style="width: 200px">
                </div>
                <div class="col-lg-6 text-light pt-4 py-2">
                    <h1>Take A Quick Survey</h1>
                    <p class="light-300">To get started, simply click the button below or scan the QR code.</p>
                    <a href="survey.php" class="btn rounded-pill btn-outline-primary shadow" style="background-color: #002d32; color: white">Click here</a>                   
		</div>
                <div class="col-lg-3 text-light align-items-center">
                    <img src="assets/img/NHA-GEHP-Survey.png" style="width: 180px">
                </div>
            </div>
        </div>
    </section>
    <!-- End Helping You Build Your Dream banner -->   

    <!-- Start Our Work -->
    <section class="container py-5">
        <div class="row justify-content-center my-5">
            <div class="filter-btns shadow-md rounded-pill text-center col-auto">
                <a class="filter-btn btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4 active" data-filter=".project" href="#">All</a>
                <a class="filter-btn btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4" data-filter=".Announcement" href="#">Announcement</a>
                <a class="filter-btn btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4" data-filter=".News" href="#">News</a>
                <a class="filter-btn btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4" data-filter="#" href="archive.php" hidden>Archive</a>
            </div>
        </div>

        <div class="row projects gx-lg-5">
        <?php
        //Bulletin
        try{
            $dbhbullet = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
            $dbhbullet->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $bulletinselect = "SELECT * FROM bulletin";
            $sthbullet = $dbhbullet->prepare($bulletinselect);
            $sthbullet->execute();
            $sthbullet->setFetchMode(PDO::FETCH_ASSOC);
            while ($bulletinrow = $sthbullet->fetch(PDO::FETCH_ASSOC))  { 
                $bulletin_id = $bulletinrow["bulletin_id"];
                $bulletin_cat = $bulletinrow["bulletin_category"];
                $bulletin_title = $bulletinrow["bulletin_title"];
                $bulletin_desc = strlen($bulletinrow["bulletin_desc"]) > 100 ? substr($bulletinrow["bulletin_desc"],0,100)."..." : $bulletinrow["bulletin_desc"];
                $bulletin_img = $bulletinrow["bulletin_image"];
                $bulletin_date = $bulletinrow["date_upload"];
        ?>
        
            <a href="news-updates.php?id=<?php echo $bulletin_id ?>" class="col-sm-6 col-lg-4 text-decoration-none py-4 project <?php echo $bulletin_cat ?>">
                <div class="service-work overflow-hidden card mb-5 mx-5 m-sm-0">
                    <img class="card-img-top" src="gehpbackend/pages/forms/uploads/bulletin/<?php echo $bulletin_img ?>" alt="..." style="max-height:395px;">
                    <div class="card-body" style="min-height: 250px; max-height: 250px;">
                        <h5 class="card-title text-dark semi-bold"><?php echo strtoupper ($bulletin_title) ?></h5>
                        <p class="card-text light-300 text-dark">
                        <?php echo $bulletin_desc ?>
                        </p>
                        <span class="text-decoration-none text-primary light-300" style="position: absolute; bottom: 25px; left: 16px;">
                              Read more <i class='bx bxs-hand-right ms-1'></i>
                          </span>
                    </div>
                </div>
            </a>
        
        
        <?php
            }
        $dbhbullet = null;
        }
        catch(PDOException $e){
            error_log('PDOException - ' . $e->getMessage(), 0);
            http_response_code(500);
            die('Error establishing connection with database');
        }
        ?>

        </div>    
    </section>
    <!-- End Our Work -->

    <!-- Start Ready To Appy -->
    <section class="bg-secondary">
        <div class="container py-5">
            <div class="row d-flex justify-content-center text-center">
                <div class="col-lg-2 col-12 text-light align-items-center">
                    <i class='display-1 bx bxs-file bx-lg'></i>
                </div>
                <div class="col-lg-7 col-12 text-light">
                    <h1>Apply Now!</h1>
                    <p class="light-300">Read the requirements carefully before filling out the Application form and submitting your application.</p>
                </div>
                <div class="col-lg-3 col-12 pt-4">
                    <a href="how-to-apply.php" class="btn btn-outline-primary rounded-pill bg-primary text-light btn-block shadow px-4 py-2">Click Here</a>
                </div>            
		</div>
        </div>
    </section>
    <!-- End Ready To Appy -->
    
    <!-- Start Feature Projects -->


    <?php
    //Featured Project
    try{
        $dbhfeatured= new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbhfeatured->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $featuredselect = "SELECT * FROM featured_project";
        $sthfeatured = $dbhfeatured->prepare($featuredselect);
        $sthfeatured->execute();
        $sthfeatured->setFetchMode(PDO::FETCH_ASSOC);
        while ($featuredrow = $sthfeatured->fetch(PDO::FETCH_ASSOC))  { 
            $featured_title = $featuredrow["featured_title"];
            $featured_desc = $featuredrow["featured_desc"];
            $featured_img1 = $featuredrow["featured_image1"];
            $featured_img2 = $featuredrow["featured_image2"];
            $featured_img3 = $featuredrow["featured_image3"];
            $featured_img4 = $featuredrow["featured_image4"];
        }
        $dbhposted = null;
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
    ?>


    <section class="bg-light py-5">
        <div class="feature-work container my-4">
            <div class="row d-flex d-flex align-items-center">
                <div class="col-lg-5">
                    <h3 class="feature-work-title h4 text-muted light-300">Featured Projects</h3>
                    <h1 class="feature-work-heading h2 py-3 semi-bold-600"><?php echo $featured_title ?></h1>
                    <p class="feature-work-body text-muted light-300">
                    <?php echo $featured_desc ?>
                    </p>
                </div>
                <div class="col-lg-6 offset-lg-1 align-left">
                    <div class="row">
                        <a class="col team-member" data-type="image">
                            <img class="img-fluid team-member-img" src="gehpbackend/pages/forms/uploads/featuredProject/<?php echo $featured_img1 ?>">
                        </a>
                        <a class="col team-member" data-type="image">
                            <img class="img-fluid team-member-img" src="gehpbackend/pages/forms/uploads/featuredProject/<?php echo $featured_img2 ?>">
                        </a>
                    </div>
                    <div class="row pt-4">
                        <a class="col team-member" data-type="image">
                            <img class="img-fluid team-member-img" src="gehpbackend/pages/forms/uploads/featuredProject/<?php echo $featured_img3 ?>">
                        </a>
                        <a class="col team-member" data-type="image">
                            <img class="img-fluid team-member-img" src="gehpbackend/pages/forms/uploads/featuredProject/<?php echo $featured_img4 ?>">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Feature Projects -->
</div>
<?php include ('assets/pages/footer.php') ?>
<!-- Lightbox -->
<script src="assets/js/fslightbox.js"></script>
<script>
	fsLightboxInstances['gallery'].props.loadOnlyCurrentSource = true;
</script>
<script>
        $(window).load(function() {
            // init Isotope
            var $projects = $('.projects').isotope({
                itemSelector: '.project',
                layoutMode: 'fitRows'
            });
            $(".filter-btn").click(function() {
                var data_filter = $(this).attr("data-filter");
                $projects.isotope({
                    filter: data_filter
                });
                $(".filter-btn").removeClass("active");
                $(".filter-btn").removeClass("shadow");
                $(this).addClass("active");
                $(this).addClass("shadow");
                return false;
            });
        });
    </script>