<?php
  $projectID = $_POST["project"];
  include("../dbcon.php");
  try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pro_avail_sql = "SELECT PJ_CODE, PJ_NAME FROM project_info WHERE PJ_CODE = :projectID AND availability = 'Available'";
    $sthpro_avail_sql = $dbh->prepare($pro_avail_sql);
    $sthpro_avail_sql->bindParam(':projectID', $projectID);
    $sthpro_avail_sql->execute();
    $sthpro_avail_sql->setFetchMode(PDO::FETCH_ASSOC); 
    while ($pro_avail = $sthpro_avail_sql->fetch(PDO::FETCH_ASSOC)) {
    }
    if($sthpro_avail_sql->rowCount() > 0) {
      echo "<script>document.getElementById('project').style.border = '';</script>";
      echo "<script>document.getElementById('submit_btn').disabled = false;</script>";
    }
    else{
      echo "<span style='color:red'>This project is currently not available due to large number of applications received. New registrations will not 
      be accommodated as of the moment. Sorry for the inconvenience.</span>";
      echo "<script>document.getElementById('project').style.border = '2px solid red';</script>";
      echo "<script>document.getElementById('submit_btn').disabled = true;</script>";
    }
    $dbh = null;
  }
  catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
  }
?>