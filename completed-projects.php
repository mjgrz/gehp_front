<?php include ('assets/pages/header.php') ?>
<?php include ('assets/pages/banner/banner-proj.php') ?>
    
    <section class="container overflow-hidden py-5">
    <!-- Start Header Page-->
    <section class="container">
        <div class="row pb-4">
            <div class="col-lg-4">
                <div class="h1 pb-4 typo-space-line">Completed Projects</div>
                        <p class="text-muted light-300">You are currently viewing NHA-GEHP Completed Projects</p>
            </div>
            <!-- Start Project Category Button -->
            <div class="col-lg-8 ">
                <div class="" method="post">
                   <div class="row justify-content-center my-2">
                    <div class="shadow-m-\d rounded-pill text-left col-auto">
                        <a class="btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4 active" href="completed-projects.php">COMPLETED PROJECTS</a>
                        <a class="btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4" href="ongoing-projects.php">ONGOING PROJECTS</a>
                        <a class="btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4 " href="upcoming-projects.php" hidden>UPCOMING PROJECTS</a>
                    </div>
                </div>
                </div>
            </div>
            <!-- End Project Category Button-->
        </div>
    </section>
    <!-- End Header Page -->
        <div class="row justify-content-center my-5">
            <div class="filter-btns shadow-md rounded-pill text-center col-auto">
                <a class="filter-btn btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4 active" data-filter=".project" href="#">All</a>
                <a class="filter-btn btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4" data-filter=".Luzon" href="#">Luzon</a>
                <a class="filter-btn btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4" data-filter=".Visayas" href="#">Visayas</a>
                <a class="filter-btn btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4" data-filter=".Mindanao" href="#">Mindanao</a>
            </div>
        </div>

        <div class="row gx-5 gx-sm-3 gx-lg-5 gy-lg-5 gy-3 pb-3 projects">

            <?php
            include("../dbcon.php"); 
            $project_status = "Completed";
            $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $select = "SELECT project_info.PJ_CODE, project_info.project_id, region.Region, project_info.PJ_NAME, units_floor.location,
                       main_image, project_info.island, project_info.availability
                       FROM project_info 
                       LEFT JOIN units_floor ON project_info.project_id = units_floor.project_id
                       LEFT JOIN region ON project_info.region = region.psgc_reg
                       WHERE project_status = :project_status AND project_info.archived != '1'
                       ORDER BY PJ_NAME";
            $sthselect = $dbh->prepare($select);
            $sthselect->bindParam(':project_status', $project_status);
            $sthselect->execute();
            $sthselect->setFetchMode(PDO::FETCH_ASSOC); 
            while ($row = $sthselect->fetch(PDO::FETCH_ASSOC)) {
            ?>
            <!-- Start Recent Work -->
            <div class="col-xl-4 col-md-4 col-sm-6 project <?php echo $row['island']; ?>">
                <a style="height: 400px; min-height: 400px;" href="project-detail.php?pn_id=<?php echo $row['project_id']; ?>" class="service-work card border-0 text-white shadow-sm overflow-hidden mx-5 m-sm-0">
                    <?php //if($row['availability'] == 'Available'){}else{include('ribbon.php');} ?>
                    <img class="service card-img" src="gehpbackend/pages/forms/uploads/projects/main_image/<?php echo $row['main_image']; ?>" alt="Card image">
                    <div class="service-work-vertical card-img-overlay d-flex align-items-end">
                        <div class="service-work-content text-left text-light">
                            <p class="card-text"><b><?php echo $row["PJ_NAME"]; ?></b> <br><span> <?php echo $row["Region"]." | ".$row["location"] ?></span></p>
                            
                        </div>
                    </div>
                </a>
            </div>
            <!-- End Recent Work -->
            <?php } ?>
            
        </div>   
    </section>
    <!-- End Posting -->

    <br><br> 



<?php include ('assets/pages/footer.php') ?>
<script>
        $(window).load(function() {
            // init Isotope
            var $projects = $('.projects').isotope({
                itemSelector: '.project',
                layoutMode: 'fitRows'
            });
            $(".filter-btn").click(function() {
                var data_filter = $(this).attr("data-filter");
                $projects.isotope({
                    filter: data_filter
                });
                $(".filter-btn").removeClass("active");
                $(".filter-btn").removeClass("shadow");
                $(this).addClass("active");
                $(this).addClass("shadow");
                return false;
            });
        });
    </script>