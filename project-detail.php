<?php include ('assets/pages/header.php') ?>

    <?php
    include("../dbcon.php"); 
    $project_id = $_GET['pn_id'];
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $select = "SELECT * FROM project_info
               LEFT JOIN units_floor ON project_info.project_id = units_floor.project_id 
               LEFT JOIN nearby_estab ON project_info.project_id = nearby_estab.project_id
               LEFT JOIN other_files ON project_info.project_id = other_files.project_id
               WHERE project_info.project_id = :project_id";
    $sthselect = $dbh->prepare($select);
    $sthselect->bindParam(':project_id', $project_id);
    $sthselect->execute();
    $sthselect->setFetchMode(PDO::FETCH_ASSOC); 
    while ($row = $sthselect->fetch(PDO::FETCH_ASSOC)) {
        $pjname = $row['PJ_NAME'];
        $availability = $row['availability'];
        $pjoverv = $row['overview'];
        $pjest1 = $row['estab1'];
        $pjest2 = $row['estab2'];
        $pjest3 = $row['estab3'];
        $pjest4 = $row['estab4'];
        $pjest5 = $row['estab5'];
        $pjest6 = $row['estab6'];
        $pjmainimg = $row['main_image'];
        $pjloc = $row['location'];
        $pjbed = $row['bedroom'];
        $pjbath = $row['bathroom'];
        $pjlotArea = $row['lot_area'];
        $pjfloArea = $row['floor_area'];
        $pjspecs = $row['specification'];
        $pjfloImg1 = $row['floor_img1'];
        $pjfloImg2 = $row['floor_img2'];
        $pjlocMap = $row['location_map'];
        $pjvinMap = $row['vicinity_map'];
        $pjuimg1 = $row['unit_img1'];
        $pjuimg2 = $row['unit_img2'];
        $pjuimg3 = $row['unit_img3'];
        $pjuimg4 = $row['unit_img4'];
        $pjuimg5 = $row['unit_img5'];
        $pjuimg6 = $row['unit_img6'];
        $videolink = $row['video'];
    } 
    
    $target_dirmain = "gehpbackend/pages/forms/uploads/projects/main_image/";
    $target_filemain = $target_dirmain . basename("$pjmainimg");
    ?>

    <!-- Start Banner Hero for Project Details-->
    <section class="bg-light w-100";>
        <div class="container">
            <div class="row d-flex align-items-center py-5">
                <div class="col-lg-8 text-start">
                    <h1 class="h2 py-5 text-primary typo-space-line"><?php echo $pjname; ?></h1>
                </div>
                <?php if($availability == 'Available'){}else{include('project_warn.php');} ?>
            </div>
        </div>
    </section>
    <!-- End Banner for About -->

    <!-- Start Project Detail -->
    <section class="container py-5">
        <div class="row justify-content-left pb-4">
            <div class="col-lg-5">
                <div id="templatemo-slide-link-target" class="card mb-3">
                    <?php
                    if(!file_exists($target_filemain)){
                    echo "No image available.";
                    }
                    else{
                    ?>
                    <img class="img-fluid border rounded" src="gehpbackend/pages/forms/uploads/projects/main_image/<?php echo $pjmainimg; ?>" alt="Card image cap">
                    <?php
                    }
                    ?>
                </div>
                <div class="worksingle-slide-footer row"> </div>
            </div>
            <div class="col-lg-7">
                <div>
                    <div class="h3 text-primary">Project Overview</div>
                    <p> <?php echo $pjoverv; ?> <br><br><br> </p>
                </div>
                <div>
                    <div class="h3 text-primary">Nearby Establishment</div>
                    <ul class="text-left list-unstyled text-primary" style="font-size: 18px;">
                        <li><i class="bx bxs-circle me-2"></i> <?php echo $pjest1; ?> </li>
                        <li><i class="bx bxs-circle me-2"></i> <?php echo $pjest2; ?> </li>
                        <li><i class="bx bxs-circle me-2"></i> <?php echo $pjest3; ?> </li>
                        <li><i class="bx bxs-circle me-2"></i> <?php echo $pjest4; ?> </li>
                        <li><i class="bx bxs-circle me-2"></i> <?php echo $pjest5; ?> </li>
                        <li><i class="bx bxs-circle me-2"></i> <?php echo $pjest6; ?> </li>
                    </ul>
                </div>
            </div>
        </div> 
        
        <!-- Start Units and Floor Plans -->
        <div class=" row gx-sm-6">  
            <div><hr style="height: 2px; background-color: #67a2b2; opacity: 100%"></div>
            <div class="col-xl-6 col-md-4 col-sm-6">
                    <div class="text-primary">
                    <div class="h3 text-primary py-2">Units and Floor Plans</div>
                    <div>
                    <ul class="text-left list-unstyled text-primary" style="font-size: 18px;">
                        <li><i class="bx bxs-location-plus me-2"></i><b class="me-3">Location: </b><?php echo $pjloc; ?></li>
                        <li><i class="bx bxs-bed me-2"></i><b class="me-3">Bedroom: </b><?php echo $pjbed; ?></li>
                        <li><i class="bx bxs-bath me-2"></i><b class="me-3">Bathroom: </b><?php echo $pjbath; ?></li>
                        <li><i class="bx bxs-area me-2"></i><b class="me-3">Lot Area: </b><?php echo $pjlotArea; ?></li>
                        <li><i class="bx bxs-square me-2"></i><b class="me-3">Floor Area: </b><?php echo $pjfloArea; ?></li>
                        <li><i class="bx bxs-home me-2"></i><b class="me-3">Specifications: </b><?php echo $pjspecs; ?></li>
                    </ul>
                </div>
                </div>
            </div>
            <?php
            $target_dirflo = "gehpbackend/pages/forms/uploads/projects/units/";
            $target_fileflo1 = $target_dirflo . basename("$pjfloImg1");
            $target_fileflo2 = $target_dirflo . basename("$pjfloImg2");
            ?>
            <div class="col-xl-3 col-md-2 col-sm-6 project py-3">
                <?php
                if(!file_exists($target_fileflo1)){
                    echo "No image available for floor plan #1.";
                }
                else{
                ?>
                <a data-type="image" data-fslightbox="floor" href="gehpbackend/pages/forms/uploads/projects/units/<?php echo $pjfloImg1; ?>">
                    <img class="img-fluid" src="gehpbackend/pages/forms/uploads/projects/units/<?php echo $pjfloImg1; ?>">
                </a>
                <?php
                }
                ?>
            </div>
            <div class="col-xl-3 col-md-2 col-sm-6 project py-3">
                <?php
                if(!file_exists($target_fileflo2)){
                    echo "No image available for floor plan #2.";
                }
                else{
                ?>
                <a data-type="image" data-fslightbox="floor" href="gehpbackend/pages/forms/uploads/projects/units/<?php echo $pjfloImg2; ?>">
                    <img class="img-fluid" src="gehpbackend/pages/forms/uploads/projects/units/<?php echo $pjfloImg2; ?>">
                </a>
                <?php
                }
                ?>
            </div>
        </div>
        <!-- Start Units and Floor Plans -->

        <br><br>

        <!-- Start Maps -->
        <div><hr style="height: 2px; background-color: #67a2b2; opacity: 100%"></div>
            <div class="col-lg-12">
                    <div class="text-primary py-2">
                    <div class="h3 typo-space-line py-2"><i class="bx bxs-map-alt me-2"></i><span class="iconify" data-inline="false"></span>Maps</div>
                    <div>
                        <div class="col-lg-12">
                            <div class="row">
                                <?php
                                $target_dirmap = "gehpbackend/pages/forms/uploads/projects/maps/";
                                $target_pjlocMap = $target_dirmap . basename("$pjlocMap");
                                $target_pjvinMap = $target_dirmap . basename("$pjvinMap");
                                ?>
                                <div class="col-lg-6 py-4"><!-- Location Map - Left Side -->
                                    <?php
                                    if(!file_exists($target_pjlocMap)){
                                        echo "No image available for location map.";
                                    }
                                    else{
                                    ?>
                                    <a class="col-lg-6" data-fslightbox="map" data-type="image" href="gehpbackend/pages/forms/uploads/projects/maps/<?php echo $pjlocMap; ?>">
                                        <div>
                                        <img class="img-fluid" src="gehpbackend/pages/forms/uploads/projects/maps/<?php echo $pjlocMap; ?>">
                                        <label class="text-muted">Location Map</label>
                                        </div>
                                    </a>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-lg-6 py-4"><!-- Vicinity Map - Right Side -->
                                    <?php
                                    if(!file_exists($target_pjvinMap)){
                                        echo "No image available for vicinity map.";
                                    }
                                    else{
                                    ?>
                                    <a class="col-lg-6" data-fslightbox="map" data-type="image" href="gehpbackend/pages/forms/uploads/projects/maps/<?php echo $pjvinMap; ?>">
                                        <div>
                                        <img class="img-fluid" src="gehpbackend/pages/forms/uploads/projects/maps/<?php echo $pjvinMap; ?>">
                                        <label class="text-muted">Vicinity Map</label>
                                        </div>
                                    </a>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <!-- End Maps -->

        <br><br>
                                    
        <!-- Start Photo Gallery -->
        <?php 
        $target_dirgal = "gehpbackend/pages/forms/uploads/projects/gallery/";
        $target_pjuimg1 = $target_dirgal . basename("$pjuimg1");
        $target_pjuimg2 = $target_dirgal . basename("$pjuimg2");
        $target_pjuimg3 = $target_dirgal . basename("$pjuimg3");
        $target_pjuimg4 = $target_dirgal . basename("$pjuimg4");
        $target_pjuimg5 = $target_dirgal . basename("$pjuimg5");
        $target_pjuimg6 = $target_dirgal . basename("$pjuimg6");
        $galleryhide = "";
        if(!file_exists($target_pjuimg1) && !file_exists($target_pjuimg2) && !file_exists($target_pjuimg3)
        && !file_exists($target_pjuimg4) && !file_exists($target_pjuimg5) && !file_exists($target_pjuimg6)) {
            $galleryhide = "hidden";
        }
        ?>
        <hr <?php echo $galleryhide; ?> style="height: 2px; background-color: #67a2b2; opacity: 100%">
        <div <?php echo $galleryhide; ?> class="h3 text-primary typo-space-line py-2"><i class="bx bxs-image-alt me-2"></i>Photo Gallery</div>
            <div>
                <div class="container">
                <div class="recent-work-header row">
                <div class="feature-work container py-2">
                    <div class="row d-flex d-flex py-3">
                        <div class="col-lg-12">
                            <div class="row">
                                <?php
                                if(!file_exists($target_pjuimg1)){
                                    echo "";
                                }
                                else{
                                ?>
                                <a class="col-lg-2" data-type="image" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg1; ?>">
                                    <img class="img-fluid" src="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg1; ?>">
                                </a>
                                <?php
                                }
                                ?>

                                <?php
                                if(!file_exists($target_pjuimg2)){
                                    echo "";
                                }
                                else{
                                ?>
                                <a class="col-lg-2" data-type="image" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg2; ?>">
                                    <img class="img-fluid" src="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg2; ?>">
                                </a>
                                <?php
                                }
                                ?>
                                
                                <?php
                                if(!file_exists($target_pjuimg3)){
                                    echo "";
                                }
                                else{
                                ?>
                                <a class="col-lg-2" data-type="image" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg3; ?>">
                                    <img class="img-fluid" src="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg3; ?>">
                                </a>
                                <?php
                                }
                                ?>
                                
                                <?php
                                if(!file_exists($target_pjuimg4)){
                                    echo "";
                                }
                                else{
                                ?>
                                <a class="col-lg-2" data-type="image" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg4; ?>">
                                    <img class="img-fluid" src="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg4; ?>">
                                </a>
                                <?php
                                }
                                ?>
                                   
                                <?php
                                if(!file_exists($target_pjuimg5)){
                                    echo "";
                                }
                                else{
                                ?>
                                <a class="col-lg-2" data-type="image" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg5; ?>">
                                    <img class="img-fluid" src="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg5; ?>">
                                </a>
                                <?php
                                }
                                ?>
                                    
                                <?php
                                if(!file_exists($target_pjuimg6)){
                                    echo "";
                                }
                                else{
                                ?>
                                <a class="col-lg-2" data-type="image" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg6; ?>">
                                    <img class="img-fluid" src="gehpbackend/pages/forms/uploads/projects/gallery/<?php echo $pjuimg6; ?>">
                                </a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
            </div>
            <!-- End Photo Gallery -->

            <!-- Start Project Video -->
            <div>  
                <?php 

                if(substr($videolink, 0, strpos($videolink, "=")) == "https://www.youtube.com/watch?v"){
                    $displayvideolink = "https://www.youtube.com/embed/".substr($videolink, strpos($videolink, "=") + 1);
                    echo '<div>
                            <hr style="height: 2px; background-color: #67a2b2; opacity: 100%">
                          </div>
                          <div class="h3 text-primary typo-space-line pb-5 py-2"><i class="bx bxs-videos me-2"></i>Walkthrough</div>';
                    echo '<iframe id="video" width="100%" height="600vh" src="'.$displayvideolink.'" title="" frameborder="0" onload="onMyFrameLoad(this)"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                          </iframe>';
                }
                else{
                    $target_dirvid = "gehpbackend/pages/forms/uploads/projects/videos/";
                    $target_pjvideo = $target_dirvid . basename("$videolink");
                    if(file_exists($target_pjvideo)){
                ?>
                    <div>
                        <hr style="height: 2px; background-color: #67a2b2; opacity: 100%">
                    </div>
                    <div class="h3 text-primary typo-space-line pb-5 py-2"><i class="bx bxs-videos me-2"></i>Walkthrough</div>

                    <video controls class="video" id="video" style="width: 100%; border-radius: 5px;">
                    <source src="gehpbackend/pages/forms/uploads/projects/videos/<?php echo $videolink ?>" type="video/mp4">
                    </video>
                <?php
                    }
                    else{

                    }
                }
                ?>
            </div>
            <!-- End Project Video -->  
        </div>
    </section>
    <!-- End Project Detail -->
    
    <div>
    	<script src="assets/js/fslightbox.js"></script>
    	<script>
        fsLightboxInstances['gallery'].props.loadOnlyCurrentSource = true;
    	</script>
        <?php include ('assets/pages/footer.php') ?>
    </div>