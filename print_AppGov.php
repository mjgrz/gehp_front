

<?php

    
    $payment = '';
    $type = '';
    $employee = '';
    $PJ_CODE = '';
    $PJ_NAME = '';
    $LNAME = '';
    $FNAME = '';
    $MNAME = '';
    $motherSurname = '';
    $ADDR1 = '';
    $email = '';
    $BDATE = '';
    $birthplace = '';
    $CSTATUS = '';
    $citizenship = '';
    $contact = '';
    $office = '';
    $tin = '';
    $pagibig = '';
    $SLNAME = '';
    $SFNAME = '';
    $SMNAME = '';
    $SmotherSurname = '';
    $SBDATE = '';
    $Sbirthplace = '';
    $nature = '';
    $EMPLOYER = '';
    $service_branch = '';
    $uni_rank = '';
    $serial_no = '';
    $POSITION = '';
    $assign_place = '';
    $mother_unit = '';
    $salary_gradeSG = '';
    $MINCOME = '';
    $dateHired = '';
    $years = '';
    $yearlabel = '';


    $APP_ID = '';
    $app_date = '';
    $applicant_id = '';
    $spouse_id = '';
    $employment_id = '';
    $dependent_id = '';
    $app_status = '';
    $pdfFile = '';
    $pdfFilefinal = '';

    $fam_income = '';
    $non_violation = '';
 

?>




<?php



// Include the main TCPDF library (search for installation path).
require_once('TCPDF-main/tcpdf.php');


// create new PDF document
$pdf = new TCPDF('p', 'mm', 'A4', true, 'UTF-8', false);


// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('GEHP');
$pdf->SetTitle('NHA | Government Employees Housing Program');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------


// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

    $imageFile = K_PATH_IMAGES. 'header.png';
    $pdf->Image($imageFile, 10, 5, 189, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

    $pdf->SetY(-272);
    $pdf->Ln(5);
    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetX(79);
    $pdf->Cell(167, 1, $app_date, 0, 1, 'C');
    $pdf->SetY(-266);
    $pdf->SetX(79);
    $pdf->Cell(167, 1, '______________________', 0, 1, 'C');
    $pdf->SetX(79);
    $pdf->Cell(167, 1, 'Date of Application', 0, 1, 'C');
    $pdf->SetFont('helvetica', 'B', 12);
    $pdf->SetY(-254);
    $pdf->SetX(10);
    $pdf->MultiCell(189, 10, 'APPLICATION TO PURCHASE A HOUSE AND LOT PACKAGE/HOUSING UNIT
    UNDER THE GOVERNMENT EMPLOYEES HOUSING PROGRAM
    ', 0, 'C', 0 , 1, '', '', true);
    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->SetY(-240);
    $pdf->Cell(167, 1, 'THE REGIONAL MANAGER', 0, 1, 'L');
    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(167, 1, 'Thru:  The District Manager', 0, 1, 'L');
    $pdf->SetY(-228);
    $pdf->Cell(167, 1, 'Sir:/Madame:', 0, 1, 'L');
    $pdf->SetY(-222);
    $pdf->MultiCell(180, 10, '     In accordance with NHA rules and regulations which I and my family agree to comply with faithfully, I hereby apply: (please check)
    ', 0, 'J', 0 , 1, '', '', true);
    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 0, 'A.1.  FOR PURCHASE OF HOUSE AND LOT PACKAGE under the GEHP thru', 0, 1, 'L');
    
    //checkboxes
    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetY(-203);
    $pdf->SetX(20);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-203);
    $pdf->SetX(25);
    $pdf->Cell(50, 3, 'Cash Sale', 0, 1, 'L');
    //check
    
    

    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetY(-203);
    $pdf->SetX(60);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-203);
    $pdf->SetX(65);
    $pdf->Cell(50, 3, 'Staggered Cash', 0, 1, 'L');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetY(-203);
    $pdf->SetX(115);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-203);
    $pdf->SetX(120);
    $pdf->Cell(50, 3, 'End-User Financing (MTO-Pag-IBIG)', 0, 1, 'L');


    
    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetY(-197);
    $pdf->Cell(5, 1, 'In-House Financing (Installment Payment): ', 0, 1, 'L');

    $pdf->SetY(-197);
    $pdf->SetX(87);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-197);
    $pdf->SetX(92);
    $pdf->Cell(50, 3, 'Straight Amortization', 0, 1, 'L');

    $pdf->SetY(-197);
    $pdf->SetX(130);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-197);
    $pdf->SetX(135);
    $pdf->Cell(50, 3, 'Escalating Amortization', 0, 1, 'L');
    //END checkboxes

    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 10, 'PROJECT NAME (see attached Annex for list): ___________________________________________________', 0, 1, 'L');
    $pdf->Cell(167, 1, 'A.2.  TYPE OF HOUSING', 0, 1, 'L');
    //Selected Project
    $pdf->SetY(-190);
    $pdf->SetX(95);
    $pdf->Cell(98, 1, $PJ_NAME, 0, 1, 'C');

    //checkboxes
    $pdf->SetFont('helvetica', '', 10);
    $pdf->SetY(-174);
    $pdf->SetX(20);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-174);
    $pdf->SetX(25);
    $pdf->Cell(50, 3, '2-Storey Duplex', 0, 1, 'L');
    

   

    $pdf->SetY(-174);
    $pdf->SetX(58);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-174);
    $pdf->SetX(63);
    $pdf->Cell(50, 3, '1-Storey Duplex', 0, 1, 'L');

    $pdf->SetY(-174);
    $pdf->SetX(96);
    $pdf->Cell(5, 1, ' ', 1, 1, 'L');
    $pdf->SetY(-174);
    $pdf->SetX(101);
    $pdf->Cell(50, 3, 'Low-Rise Building/Condominium', 0, 1, 'L');
    //END checkboxes

     
        
    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 10, 'A.3. As Government Employee of   _______________________________________________ (please specify)', 0, 1, 'L');

    //Selected Project
    $pdf->SetY(-167);
    $pdf->SetX(72);
    $pdf->Cell(98, 1, $employee, 0, 1, 'C'); 



    

    
    $pdf->Ln(8);
    $pdf->Cell(167, 1, "I. APPLICANT'S IDENTITY: (For female applicant/spouse, give complete maiden name)", 0, 1, 'L');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(167, 10, 'NAME: _____________________________________________________________________________________', 0, 1, 'L');
    $pdf->SetY(-143);
    $pdf->SetX(42);
    $pdf->Cell(50, 3, '(Last)', 0, 1, 'L');
    //lastname
    $pdf->SetY(-148);
    $pdf->SetX(27);
    $pdf->Cell(41, 5, $LNAME, 0, 1, 'C');

    $pdf->SetY(-143);
    $pdf->SetX(83);
    $pdf->Cell(50, 3, '(First)', 0, 1, 'L');
    //firstname
    $pdf->SetY(-148);
    $pdf->SetX(68);
    $pdf->Cell(41, 5, $FNAME, 0, 1, 'C');

    $pdf->SetY(-143);
    $pdf->SetX(122);
    $pdf->Cell(50, 3, '(Middle)', 0, 1, 'L');
    //middlename
    $pdf->SetY(-148);
    $pdf->SetX(109);
    $pdf->Cell(41, 5, $MNAME, 0, 1, 'C');

    $pdf->SetY(-143);
    $pdf->SetX(155);
    $pdf->Cell(50, 3, "(Mother's Surname)", 0, 1, 'L');
    //Mother's Surname
    $pdf->SetY(-148);
    $pdf->SetX(151);
    $pdf->Cell(40, 5, $MNAME, 0, 1, 'C');
    
    $pdf->Ln(5);
    $pdf->Cell(167, 5, 'Residence/Address: ___________________________________________________________________________', 0, 1, 'L');
    //address
    $pdf->SetY(-138);
    $pdf->SetX(49);
    $pdf->Cell(144, 5, $ADDR1, 0, 1, 'C');

    $pdf->Cell(167, 5, 'Date of Birth:_____________ Place of Birth ____________________________________ Civil Status___________', 0, 1, 'L');
    //bdate
    $pdf->SetY(-133);
    $pdf->SetX(37);
    $pdf->Cell(25, 5, $BDATE, 0, 1, 'C');
    //bplace
    $pdf->SetY(-133);
    $pdf->SetX(86);
    $pdf->Cell(68, 5, $birthplace, 0, 1, 'C');
    //civil status
    $pdf->SetY(-133);
    $pdf->SetX(175);
    $pdf->Cell(20, 5, $CSTATUS, 0, 1, 'C');

    $pdf->Cell(167, 5, 'Citizenship: __________ Contact Numbers (Residence) _____________________ Office ____________________', 0, 1, 'L');
    //citizenship
    $pdf->SetY(-128);
    $pdf->SetX(35);
    $pdf->Cell(20, 5, $citizenship, 0, 1, 'C');
    //contact
    $pdf->SetY(-128);
    $pdf->SetX(105);
    $pdf->Cell(40, 5, $contact, 0, 1, 'C');
    //office
    $pdf->SetY(-128);
    $pdf->SetX(156);
    $pdf->Cell(40, 5, $office, 0, 1, 'C');

    $pdf->Cell(167, 5, 'TIN No. _______________________________ GSIS/SSS/Pag-IBIG Policy No. ____________________________', 0, 1, 'L');
    //UMID
    $pdf->SetY(-123);
    $pdf->SetX(35);
    $pdf->Cell(50, 5, $tin, 0, 1, 'C');
    //pagibig
    $pdf->SetY(-123);
    $pdf->SetX(142);
    $pdf->Cell(50, 5, $pagibig, 0, 1, 'C');

    $pdf->Cell(167, 5, 'Name of Spouse: _____________________________________________________________________________', 0, 1, 'L');
    $pdf->SetY(-114);
    $pdf->SetX(57);
    $pdf->Cell(50, 5, '(Last)', 0, 1, 'L');
    //SLNAME
    $pdf->SetY(-118);
    $pdf->SetX(44);
    $pdf->Cell(37, 5, $SLNAME, 0, 1, 'C');

    $pdf->SetY(-114);
    $pdf->SetX(94);
    $pdf->Cell(50, 5, '(First)', 0, 1, 'L');
    //SFNAME
    $pdf->SetY(-118);
    $pdf->SetX(81);
    $pdf->Cell(37, 5, $SFNAME, 0, 1, 'C');

    $pdf->SetY(-114);
    $pdf->SetX(131);
    $pdf->Cell(50, 5, '(Middle)', 0, 1, 'L');
    //SMNAME
    $pdf->SetY(-118);
    $pdf->SetX(120);
    $pdf->Cell(37, 5, $SMNAME, 0, 1, 'C');

    $pdf->SetY(-114);
    $pdf->SetX(160);
    $pdf->Cell(50, 5, "(Mother's Surname)", 0, 1, 'L');
    //Mother's Surname
    $pdf->SetY(-118);
    $pdf->SetX(157);
    $pdf->Cell(37, 5, $SmotherSurname, 0, 1, 'C');


    $pdf->Ln(4);
    $pdf->Cell(167, 5, 'Date of Birth__________________________________ Place of Birth____________________________________', 0, 1, 'L');
    //SBDATE
    $pdf->SetY(-109);
    $pdf->SetX(38);
    $pdf->Cell(60, 5, $SBDATE, 0, 1, 'C');
    //Sbirthplace
    $pdf->SetY(-109);
    $pdf->SetX(127);
    $pdf->Cell(60, 5, $Sbirthplace, 0, 1, 'C');


    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 10, 'II. APPLICANT’S EMPLOYMENTS STATUS', 0, 1, 'L');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(167, 3, 'Nature of Employment: ______________________', 0, 1, 'L');
    $pdf->Cell(167, 3, 'Name of Agency/ Corporation: ___________________________________', 0, 1, 'L');
    //Address
    $pdf->SetY(-94.5);
    $pdf->SetX(132);
    $pdf->Cell(40, 3, 'Address: __________________________________', 0, 1, 'C');
    //agency
    $pdf->SetY(-90);
    $pdf->SetX(64);
    $pdf->Cell(70, 3, $EMPLOYER, 0, 1, 'C');

    $pdf->SetY(-74.7);
    $pdf->SetX(110);
    $pdf->Cell(70, 3, 'Serial / Badge No.:_______________', 0, 1, 'L');

    $pdf->SetY(-70.6);
    $pdf->SetX(110);
    $pdf->Cell(70, 3, 'Branch of Service:___________________________', 0, 1, 'L');
    
    $pdf->SetY(-66.2);
    $pdf->SetX(110);
    $pdf->Cell(70, 3, 'Gross Monthly Income:_________________', 0, 1, 'L');

    $pdf->SetY(-61.8);
    $pdf->SetX(110);
    $pdf->Cell(70, 3, 'Place of Assignment:_________________________', 0, 1, 'L');

    $pdf->SetY(-57.4);
    $pdf->SetX(110);
    $pdf->Cell(70, 3, 'Mother Unit:___________________________', 0, 1, 'L');
    
    $pdf->SetY(-38.5);
    $pdf->SetX(110);
    $pdf->Cell(70, 3, 'Gross Monthly Income:_________________', 0, 1, 'L');
    

    $pdf->SetY(-85);

    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 10, 'For Military Personnel:', 0, 1, 'L');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(167, 4, 'Rank:______________________________', 0, 1, 'L');
    

    $pdf->Cell(167, 4, 'Position:________________________________', 0, 1, 'L');
    

    $pdf->Cell(167, 4, 'Salary Grade:____________', 0, 1, 'L');
    
    

    $pdf->Cell(167, 4, 'Date Hired:________________', 0, 1, 'L');
     
    

    $pdf->Cell(167, 4, 'No. of years in the Government: ___________', 0, 1, 'L');
    
    

    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 10, 'For Government Employees:', 0, 1, 'L');

    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(167, 4, 'Position:________________________________', 0, 1, 'L');
    
    

    $pdf->Cell(167, 4, 'Salary Grade:____________', 0, 1, 'L');
    

    $pdf->Cell(167, 4, 'Date Hired:________________', 0, 1, 'L');
    

    $pdf->Cell(167, 4, 'No. of years in the Government: ___________', 0, 1, 'L');


    
    
    if($uni_rank != "N/A"&&$uni_rank != "n/a"&&$uni_rank != ""){
        //Rank
        $pdf->SetY(-75);
        $pdf->SetX(26);
        $pdf->Cell(57, 3, $uni_rank, 0, 1, 'C');
        //serial_no
        $pdf->SetY(-75);
        $pdf->SetX(141);
        $pdf->Cell(28, 3, $serial_no, 0, 1, 'C');
        //Position
        $pdf->SetY(-70.5);
        $pdf->SetX(30);
        $pdf->Cell(62, 3, $POSITION, 0, 1, 'C');
        //service_branch
        $pdf->SetY(-70.5);
        $pdf->SetX(140);
        $pdf->Cell(50, 3, $service_branch, 0, 1, 'C');
        //salary_grade
        $pdf->SetY(-66);
        $pdf->SetX(38);
        $pdf->Cell(23, 3, $salary_grade, 0, 1, 'C');
        //income
        $pdf->SetY(-66);
        $pdf->SetX(148);
        $pdf->Cell(32, 3, $MINCOME, 0, 1, 'C');
        //Date
        $pdf->SetY(-61.5);
        $pdf->SetX(35);
        $pdf->Cell(29, 3, $dateHired, 0, 1, 'C');
        //assign_place
        $pdf->SetY(-61.5);
        $pdf->SetX(145);
        $pdf->Cell(47, 3, $assign_place, 0, 1, 'C');
        //years
        $pdf->SetY(-57);
        $pdf->SetX(68);
        $pdf->Cell(20, 3, $years." ".$yearlabel, 0, 1, 'C');
        //mother_unit
        $pdf->SetY(-57);
        $pdf->SetX(131);
        $pdf->Cell(50, 3, $mother_unit, 0, 1, 'C');
    }

    else{
        //Position
        $pdf->SetY(-43);
        $pdf->SetX(30);
        $pdf->Cell(62, 3, $POSITION, 0, 1, 'C');
        //salary_grade
        $pdf->SetY(-39);
        $pdf->SetX(38);
        $pdf->Cell(23, 3, $salary_grade, 0, 1, 'C');
        //income
        $pdf->SetY(-39);
        $pdf->SetX(148);
        $pdf->Cell(32, 3, $MINCOME, 0, 1, 'C');
        //Date
        $pdf->SetY(-34.5);
        $pdf->SetX(35);
        $pdf->Cell(29, 3, $dateHired, 0, 1, 'C');
        //years
        $pdf->SetY(-30);
        $pdf->SetX(68);
        $pdf->Cell(21, 3, $years." ".$yearlabel, 0, 1, 'C');
    } 
    
    
    

    $pdf->AddPage();
    $pdf->SetY(-287);
    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(167, 10, "III. APPLICANT'S FAMILY COMPOSITION (add additional page if necessary) :", 0, 1, 'L');


    

    
    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(176, 3, '                   NAME    	                       RELATION TO           CIVIL STATUS         AGE 	    EMPLOYMENT/SOURCE', 0, 1, 'L');
    $pdf->Cell(176, 3, '                                                           APPLICANT                                                                      OF INCOME', 0, 1, 'L');
    
    
        $pdf->Cell(176, 5, '_________________________   ________________    _______________   _______   _____________________', 0, 1, 'L');
        $pdf->Cell(176, 5, '_________________________   ________________    _______________   _______   _____________________', 0, 1, 'L');
        $pdf->Cell(176, 5, '_________________________   ________________    _______________   _______   _____________________', 0, 1, 'L');
        $pdf->Cell(176, 5, '_________________________   ________________    _______________   _______   _____________________', 0, 1, 'L');
       
    

    

    $pdf->SetFont('helvetica', 'B', 10);

    

    if($fam_income == ""){
        $pdf->Cell(167, 10, "IV. APPLICANT'S TOTAL FAMILY INCOME PER MONTH: (P _________________ )", 0, 1, 'L');
    }
    if($fam_income != ""){
        $pdf->Cell(167, 10, "IV. APPLICANT'S TOTAL FAMILY INCOME PER MONTH: (P ". $fam_income . " )", 0, 1, 'L');
    }

    $pdf->Cell(167, 9, "V. FAMILY REAL PROPERTY HOLDINGS: (please underline)", 0, 1, 'L');
    $pdf->SetFont('helvetica', '', 10);

    if($non_violation == "YES"){
        $value = '&nbsp;Have never availed of any form of government housing assistance, nor violated Section 14 of RA 7279 (<u>YES</u> / NO)';
    }
    if($non_violation == "NO"){
        $value = '&nbsp;Have never availed of any form of government housing assistance, nor violated Section 14 of RA 7279 (YES / <u>NO</u>)';
    }
    if($non_violation == ""){
        $value = '&nbsp;Have never availed of any form of government housing assistance, nor violated Section 14 of RA 7279 (YES / NO)';
    }
    $pdf->writeHTML($value, true, 0, true, 0);

    //$pdf->Cell(180, 9, "Have never availed of any form of government housing assistance, nor violated Section 14 of RA 7279 ( / NO)", 0, 1, 'J');

    
    

    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(176, 9, "VI. CERTIFICATIONS ", 0, 1, 'L');

    $pdf->Ln(2);
    $pdf->SetFont('helvetica', '', 10);
    $pdf->MultiCell(180, 10, '    I/We certify that the foregoing information/statement is to my/our knowledge, true, correct, complete, and updated. The signature/s appearing above my/our name/names below is/are genuine.

    I/We authorize National Housing Authority (NHA) or its duly authorized representative to verify necessary information or data (i.e. certificate of employment, pay slips and income tax return) with the concerned government agencies or third parties including banks and other financial institutions from whom NHA had obtained information.

      I/We hereby agrees that any misrepresentation of a material fact is a ground for disapproval of the application, cancellation of the loan, and shall be a cause for the total outstanding obligation to be due and demandable and shall be subject to other sanctions provided in existing NHA guidelines. I/We agree to notify NHA of any material change affecting the information contained herein. I/We agree that all information obtained by NHA) shall remain its property whether or not the loan is granted.

    I/We further agree to be bounded by the current and general policies of NHA and those that the NHA may adopt in the future, that may have relation to or in any way affect my/our loan.

    I/We understand that the processing/service/filing fee, notarial and all other incidental fees pertaining to the registration of mortgage on property shall be for my/our account.
', 0, 'J', 0 , 1, '', '', true);

    $pdf->Ln(15);
    $pdf->Cell(176, 5, "___________________________________________          ___________________________________________ ", 0, 1, 'L');
    $pdf->SetFont('helvetica', '', 9);
    $pdf->Cell(176, 5, "   SIGNATURE OVER PRINTED NAME OF BORROWER                      SIGNATURE OVER PRINTED NAME OF SPOUSE ", 0, 1, 'L');

    $pdf->Ln(5);
    $pdf->Cell(176, 5, "             __________________________________                                       __________________________________  ", 0, 1, 'L');
    $pdf->Cell(176, 5, "                                          DATE                                                                                                 DATE ", 0, 1, 'L');

    $pdf->Ln(7);
    $pdf->SetFont('helvetica', '', 10);
    $pdf->MultiCell(180, 10, '    SUBSCRIBED AND SWORN to before me in_________________________Philippines, this ______ day of _____________________ 20____, AFFIANT, having exhibited to me, his/her ID No. __________________ issued on ________________ at _________________________.
    ', 0, 'J', 0 , 1, '', '', true);

    $pdf->Ln(2);
    $pdf->Cell(176, 5, "Doc. No. __________				                                                                                         				NOTARY PUBLIC", 0, 1, 'L');
    $pdf->Cell(176, 5, "Page No. __________", 0, 1, 'L');
    $pdf->Cell(176, 5, "Book No. __________", 0, 1, 'L');
    $pdf->Cell(176, 5, "Series of 20", 0, 1, 'L');

    $pdf->Ln(9);
    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(176, 1, "Note:   Please provide complete  information and  write legibly in PRINT /BLOCK LETTERS", 0, 1, 'L');

// Clean any content of the output buffer
ob_end_clean();



// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output($pdfFilefinal.'ApplicationForm_Gov.pdf', 'D');



?>