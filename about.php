<?php include ('assets/pages/header.php') ?>

<?php
try{
    $dbhabout = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbhabout->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $aboutselect = "SELECT * FROM about";
    $sthabout = $dbhabout->prepare($aboutselect);
    $sthabout->execute();
    $sthabout->setFetchMode(PDO::FETCH_ASSOC);
    while ($aboutrow = $sthabout->fetch(PDO::FETCH_ASSOC))  { 
        $about_title = $aboutrow["about_title"];
        $about_desc = $aboutrow["about_desc"];
        $videolink = $aboutrow["about_video"];
    }
    $dbhabout = null;
}
catch(PDOException $e){
    error_log('PDOException - ' . $e->getMessage(), 0);
    http_response_code(500);
    die('Error establishing connection with database');
}
?>
<?php 
if(substr($videolink, 0, strpos($videolink, "=")) == "https://www.youtube.com/watch?v"){
    $displayvideolink = "https://www.youtube.com/embed/".substr($videolink, strpos($videolink, "=") + 1);
    echo '<iframe id="video" width="100%" height="800vh" src="'.$displayvideolink.'" title="" frameborder="0" onload="onMyFrameLoad(this)"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
          </iframe>';
}
else{
    $target_dirvid = "gehpbackend/pages/forms/uploads/aboutVideo/";
    $target_pjvideo = $target_dirvid . basename("$videolink");
    if(file_exists($target_pjvideo)){
?>
    <video controls autoplay class="video" id="video" style="width: 100%; border-radius: 5px;">
    <source src="gehpbackend/pages/forms/uploads/aboutVideo/<?php echo $videolink ?>" type="video/mp4">
    </video>
<?php
    }
    else{

    }
}
?>

   <section class="container py-3">
        <div class="row pt-5">
            <div class="content row col-10 m-auto text-left">
                <h2 class="h1 pb-4 typo-space-line"><?php echo $about_title ?></h2>
                <p class="service-footer regular-400" style="text-align:justify;">
                    <?php echo nl2br($about_desc); ?>
                </p>
            </div>
            <br>
        </div><!-- End Blog Cover -->
    </section>
    <!-- End Work Sigle -->

    <br><br> 

    <!-- Start Ready To Appy -->
    <section class="bg-secondary">
        <div class="container py-5">
            <div class="row d-flex justify-content-center text-center">
                <div class="col-lg-2 col-12 text-light align-items-center">
                    <i class='display-1 bx bxs-file bx-lg'></i>
                </div>
                <div class="col-lg-7 col-12 text-light">
                    <h2 class="h2">Ready To Apply?</h2>
                    <p class="light-300">Read the requirements and submit your application for verification.</p>
                </div>
                <div class="col-lg-3 col-12 pt-4">
                    <a href="how-to-apply.php" class="btn btn-primary rounded-pill btn-block shadow px-4 py-2">View Requirements Here</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Ready To Appy -->

    <br><br><br>

    <!-- Start Aim -->
    <section class="banner-bg bg-white py-5">
        <div class="container my-4">
            <div class="row text-center">

                <div class="objective col-lg-6">
                    <div class="objective-icon card m-auto py-4 mb-2 mb-sm-4 bg-secondary shadow-lg">
                        <i class="display-4 bx bxs-donate-heart text-light"></i>
                    </div>
                    <h2 class="objective-heading h3 mb-2 mb-sm-4">Objective</h2>
                    <p class="regular-400">
                        (A) Promote competitive and commendable units to the public, most especially to our public servants; (B) provide opportunity for all government employees to apply for housing units; and (C) to expand housing options for government employees in need of a home they can call their own.
                    </p>
                </div>

                <div class="objective col-lg-6">
                    <div class="objective-icon card m-auto py-4 mb-2 mb-sm-4 bg-secondary shadow-lg">
                        <i class="display-4 bx bx-show-alt text-light"></i>
                    </div>
                    <h2 class="objective-heading h3 mb-2 mb-sm-4">Vision of the Program</h2>
                    <p class="regular-400">
                        
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- End Aim -->

    <br><br>
<?php include ('assets/pages/footer.php') ?>