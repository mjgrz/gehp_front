<?php include ('assets/pages/header.php') ?>
<!-- Start Progress -->
    <section class="bg-white py-5">
        <div class="container my-4">
            <div class="col-lg-12">   
            <h1 class="creative-heading h2 pb-3 typo-space-line">News and Updates Archive </h1>
            </div>
            <section class="container py-5">
            <div class="row justify-content-center mb-5">
            <div class="filter-btns shadow-m-\d rounded-pill text-center col-auto">
                <a class="filter-btn btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4 active" data-filter=".update" href="#">All</a>
                <a class="filter-btn btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4" data-filter=".announcement" href="#">Announcement</a>
                <a class="filter-btn btn rounded-pill btn-outline-primary border-0 m-md-2 px-md-4" data-filter=".event" href="#">News</a>
            </div>
        </div>

        <div class="row projects gx-lg-5">
            <a href="news-updates.php" class="col-sm-6 col-lg-4 text-decoration-none py-4 project update event">
                <div class="service-work overflow-hidden card mb-5 mx-5 m-sm-0">
                    <img class="card-img-top" src="assets/img/Hidilyn-Diaz-2.jpg" alt="...">
                    <div class="card-body">
                        <h5 class="card-title text-dark semi-bold">OLYMPIC MEDALISTS RECEIVE P1.8M HOUSE AND LOT</h5>
                        <p class="card-text light-300 text-dark">
                            2020 Tokyo Olympics Gold and Bronze Medalists SSgt. Hidilyn Diaz and Sgt. Eumir Marcial were recipients of a P1.8 million fully-furnished house and lot...
                        </p>
                        <span class="text-decoration-none text-primary light-300">
                              Read more <i class='bx bxs-hand-right ms-1'></i>
                          </span>
                    </div>
                </div>            </a>
        </div>
            </section>
        </div>
    </section>
<!-- End Progress -->

<!-- Page Script -->
<?php include ('assets/pages/footer.php') ?>
    <script>
        $(window).load(function() {
            // init Isotope
            var $projects = $('.projects').isotope({
                itemSelector: '.project',
                layoutMode: 'fitRows'
            });
            $(".filter-btn").click(function() {
                var data_filter = $(this).attr("data-filter");
                $projects.isotope({
                    filter: data_filter
                });
                $(".filter-btn").removeClass("active");
                $(".filter-btn").removeClass("shadow");
                $(this).addClass("active");
                $(this).addClass("shadow");
                return false;
            });
        });
    </script>