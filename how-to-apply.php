<?php include ('assets/pages/header.php') ?>
<?php include ('assets/pages/banner/banner-req.php') ?>
<?php include ('styles.css') ?>

<!-- Start Steps and Instructions Section -->
            <section class=" pt-sm-0 py-5 shadow">
                <div class="container py-3">  


                    <!-- Start Instruction Horizontal -->
                    <div class="pricing-horizontal row col-xl-12 col-md-4 col-sm-6 d-flex rounded overflow-hidden">
                        <h1 class="h1 py-3 text-primary typo-space-line">Qualifications and Instructions</h1>
                    </div>
                    <!-- End Instruction Horizontal -->

                    <br><br>


                    <div class="row gx-5 gx-sm-3 gx-lg-5 gy-lg-5 gy-3 pb-3">
                        <!-- Start Recent Work -->
                        <div class="col-xl-4 col-md-4 col-sm-6 project mindanao">
                            <div class="card border-0 shadow " style="height: 10px; height: 300px;">
                                        <div class="service-work-content text-primary px-4 py-4">
                                            <label class="card-text h1">1</label>
                                            <p class="justify-content-center regular-400">Applicants must be a regular <b>Government Employee</b> or <b>Overseas Filipino Worker</b> (OFW).</p>
                                        </div>
                                    </div>
                        </div>
                        <!-- End Recent Work -->

                        <!-- Start Recent Work -->
                        <div class="col-xl-4 col-md-4 col-sm-6">
                            <div class="card border-0 shadow " style="height: 10px; height: 300px;">
                                        <div class="service-work-content text-primary px-4 py-4">
                                            <label class="card-text h1">2</label>
                                            <p class="justify-content-center regular-400">Must have an updated payment of account, if already an awardee of NHA.</p>
                                        </div>
                                    </div>
                        </div>
                        <!-- End Recent Work -->

                        <!-- Start Recent Work -->
                        <div class="col-xl-4 col-md-4 col-sm-6">
                            <div class="card border-0 shadow " style="height: 10px; height: 300px;">
                                        <div class="service-work-content text-primary px-4 py-4">
                                            <label class="card-text h1">3</label>
                                            <p class="justify-content-center regular-400">He/she must not be more than <b>sixty-five (65) years old</b> at the date of application and not more than <b>seventy (70) years old</b> at the time of maturity of the housing loan. </p>
                                        </div>
                                    </div>
                        </div>
                        <!-- End Recent Work -->

                        <!-- Start Recent Work -->
                        <div class="col-xl-4 col-md-4 col-sm-6 project mindanao">
                            <div class="card border-0 shadow " style="height: 10px; height: 300px;">
                                        <div class="service-work-content text-primary px-4 py-4">
                                            <label class="card-text h1">4</label>
                                            <p class="justify-content-center regular-400">If retired government employee, he/she may avail the housing benefit within one year from date of retirement and shall pay an outright cash.
                                            </p>
                                        </div>
                                    </div>
                        </div>
                        <!-- End Recent Work -->

                        <!-- Start Recent Work -->
                        <div class="col-xl-4 col-md-4 col-sm-6">
                            <div class="card border-0 shadow " style="height: 10px; height: 300px;">
                                        <div class="service-work-content text-primary px-4 py-4">
                                            <label class="card-text h1">5</label>
                                            <p class="justify-content-center regular-400">If you are eligible/qualified, you may fill up the Application Form <a href="application-form-ofw-ge.php"><b>here</b></a> using the website or download the <a href="" onclick="window.open('https://drive.google.com/file/d/1IfP36RU8jODkQw5GSVBkJXMl2FX1FGbH/view?usp=sharing')"><b>Application to Purchase House and Lot Form</b></a>. You may also download the other requirements from the list provided below.
                                   
					</div>
                                    </div>
                        </div>
                        <!-- End Recent Work -->

                        <!-- Start Recent Work -->
                        <div class="col-xl-4 col-md-4 col-sm-6">
                            <div class="card border-0 shadow " style="height: 10px; height: 300px;">
                                        <div class="service-work-content text-primary px-4 py-4">
                                            <label class="card-text h1">6</label>
                                            <p class="justify-content-center regular-400">Based on the availability of the projects listed on this website, choose your preferred housing project (<i>click</i> <a href="ongoing-projects.php"><b>GEHP Projects</b></a> <i>to check out available housing project</i>).
                                            </p>
                                        </div>
                                    </div>
                        </div>
                        <!-- End Recent Work -->

                        <div class="col-xl-4 col-md-4 col-sm-6">
                            <div class="card border-0 shadow " style="height: 10px; height: 300px;">
                                        <div class="service-work-content text-primary px-4 py-2">
                                            <label class="card-text h1">7</label>
                                            <p class="justify-content-center">For the mode of payment, the applicant may opt to pay through End-User Financing (MTO Pag-IBIG) or Cash Sale.
                                            <ul style="font-size: 13px">
                                            <li>For <b>End-User Financing (MTO-Pag-IBIG)</b>, kindly fill out NHA Forms, Pag-IBIG Forms and all requirements stated on the <b>Checklist of Requirements.</b></li>
                                            <li><b>Cash Sale/Staggered Cash/In-House Financing</b> (Installment Payment), kindly fill out NHA Forms and all requirements stated on the <b>Checklist of Requirements</b>.</li></p>
                                        </div>
                                    </div>
                        </div>

                        <div class="col-xl-4 col-md-4 col-sm-6">
                            <div class="card border-0 shadow " style="height: 10px; height: 300px;">
                                        <div class="service-work-content text-primary px-4 py-2">
                                            <label class="card-text h1">8</label>
                                            <p class="justify-content-center regular-400">Make sure your Application Form is notarized prior to the submission of documentary requirements.</p>
                                        </div>
                                    </div>
                        </div>

                        <div class="col-xl-4 col-md-4 col-sm-6">
                            <div class="card border-0 shadow " style="height: 10px; height: 300px;">
                                        <div class="service-work-content text-primary px-4 py-2">
                                            <label class="card-text h1">9</label>
                                            <p class="justify-content-center regular-400">Application forms and requirements may be submitted to the Implementing Regional/District Office of your chosen housing project or on any NHA Office near your present location.</p>
                                        </div>
                                    </div>
                        </div>
                        <!-- End Recent Work -->
                    </div>
                </div>
            </section>
            <!--End Instruction Horizontal Section-->

            <!-- START CHECKLIST OF REQUIREMENTS -->
            <section class=" pt-sm-0 py-5 bg-light">
                <div class="container py-3 pb-5">
                    <div>
                        <h2 class="h1 pb-4 typo-space-line py-5">GEHP/OFW Checklist of Requirements</h2>
                        <p class="feature-work-body text-muted regular-400">
                            Get the following requirement for NHA Government Employee Housing Program here; kindly check each section to view full details:
                        </p>
                    </div>
                    <!-- START NHA REQUIREMENTS -->
                    <div class="py-3">
                        <h5>I. NHA REQUIREMENTS</h5>
                        <div style="text-indent: 10px;">
                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Application Form</button>
                            <div class="panel py-2 bg-light">
                                <ul> 
                                    <li>Duly accomplished Application to Purchase House and Lot Form.</li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Overseas Employement Certificate</button>
                            <div class="panel py-2 bg-light">
                                <ul> 
                                    <li>For Overseas Filipino Worker, provide Overseas Employment Certificate (OEC) or in its absence, the POEA Work Contract.</li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Proof of Income. <i>Any of the following:</i></button>
                            <div class="panel py-2 bg-light">
                                <ul> 
                                    <li>Employer's <b>Certificate of Compensation</b></li>
                                    <li>BIR <b>Certified Income Tax Return</b></li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Certificate of Active in Service</button>
                            <div class="panel py-2 bg-light">
                                <ul> 
                                    <li>For Uniformed Personnel Only, provide Certificate of Active in Service.</li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Proof of Identity/Civil Status</button>
                            <div class="panel py-2 bg-light">
                                <ul> 
                                    <li><b>For Single Applicants:</b></li>
                                        <ul>
                                        <li>Birth Certificate</li>
                                        <li>Any two (2) government-issued ID</li>
                                        </ul>
                                    <li><b>For Married Applicants:</b></li>
                                        <ul>
                                        <li>Marriage Certificate</li>
                                        <li>Affidavit of Separation-in-Fact (for applicants not legally separated/annulled)</li>
                                        <li>Any two (2) government-issued ID</li>
                                        </ul>                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Government Issued IDs</button>
                            <div class="panel py-2 bg-light">
                                <ul> 
                                    <li>List of Acceptable Government-Issued ID</li>
                                    <ul>
                                        <li>Philippine National ID</li>
                                        <li>Driver's License</li>
                                        <li>Postal ID</li>
                                        <li>Voter's ID</li>
                                        <li>Passport</li>
                                        <li>GSIS/SSS/PHILHEALTH/PAG-IBIG Card/UMID</li>
                                        <li>NBI/Police Clearance/ID</li>
                                        <li>Government Office I.D. </li>
                                        <li>Barangay Certification with Picture</li>
                                        <li>DSWD Certification/ Solo Parent ID</li>
                                        <li>PRC/IBP/OWWA</li>
                                        <li>Senior Citizen ID</li>
                                        <li>Persons with Disabilities (PWD) ID</li>
                                    </ul>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->
                        </div>
                    </div>
                    <!-- END NHA REQUIREMENTS -->

                    <!-- START PAG-IBIG FUND -->
                    <div class="py-3">
                        <h5>II. PAG-IBIG FUND</h5>
                        <div style="text-indent: 10px;">
                            <div>
                                <label class="h5">A. Upon Loan Application</label>
                            </div>
                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Housing Loan Application </button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                        Duly accomplished <a href="" onclick="window.open('https://drive.google.com/file/d/1CKtuYY5-kgfVBXFoVuFlV-RMBmUKuUsZ/view?usp=sharing')">Housing Loan Application</a> with recent 1"x1" ID photo of borrowed/co-borrower (if applicable) (2 original copies) computer generated or photocopied, picture is not acceptable.
                                        <br><br>
                                        <i>Note:</i> For employers who are requiring their employees of an authorization letter allowing said employer to disclose employment information to Pag-IBIG Fund, the member applicant shall execute letter in the format being required by his/her employer.
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Proof of Income</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                        <i><u>For Locally Employed, any of the following:</u></i><br><br>

                                        <text><b>a.</b> Notarized Certificate of employment and Compensation (CEC), indicating the gross monthly income and monthly allowance and monthly monetary benefits received by the employee (1 original copy) duly signed by the authorized signatory of the employer. For system generated CEC, notarize the said CEC with signature of the authorized signatory of the employer.</text><br><br>

                                        <text><b>b.</b> Latest Income Tax Return (ITR) for the year immediately preceding the date of loan application, with attached BIR Form No. 2316, duly acknowledged by the BIR or authorized representative employer (1 photocopy).</text><br><br>

                                        <text><b>c.</b> One (1) Month Pay slip, within the last three (3) months prior of date loan application with name and signature of the following authorized signatory of the employer (1 certified true copy).<br><br>

                                        <i>Note:</i> For government employees who will be paying their loan amortization through salary deduction, the original copy of One (1) Month Pay slip, within the last three (3) months prior to date of loan application must be submitted together with CEC or ITR as mentioned above.</text>
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Valid ID with Signature</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                        One (1) valid ID with signature (1 photocopy back-to-back) of borrower and spouse, <i>if applicable.</i> The same ID must be presented during the conduct of borrower's validation.
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <br>

                            <!-- START FROM THE SELLER OR BORROWER -->
                            <div>
                                <label class="h6">From the Seller or Borrower</label>
                            </div>
                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;TCT</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                        TCT (latest /Certified True Copy). For Condominium Unit, present TCT of the land and Condominium Certificate of Title (CCT) (Certified True Copy)
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Tax Declaration</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>                                          
                                        Updated Tax Declaration (House and Lot and Updated Real Estate Tax Receipt photocopy)
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Contract-to-Sell</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>                                          
                                        Contract-to-Sell or similar agreement between buyer and seller
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Vicinity Map/Sketch Map</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>                                         
                                        Vicinity Map/Sketch Map leading to the Property subject of the loan.
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->
                            <!-- END FROM THE SELLER OR BORROWER -->

                            <br>
                            <!-- START OTHER REQUIREMENTS -->
                            <div>
                                <label class="h6">Other Requirement</label>
                            </div>
                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Health Insurance</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                        <b>Health Statement Form (Questionaire)</b><br>
                                        <div style="text-indent: 5px;">
                                            <b>a.</b> For borrowers over 60 years old </div>
                                        <div>
                                            <b>b.</b> For borrowers up to 60 years old, if loan is over P2.0M to P6.0m</div>
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->
                            <!-- END OTHER REQUIREMENTS -->

                            <br>
                            <!-- START PRIOR TO RELEASE OF LOAN PROCEEDS-->
                            <div>
                                <label class="h5">B. Prior to Release of Loan Proceeds</label>
                            </div>
                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;TCT/CCT</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                        TCT/CCT covering the subject property in the name of Pag-IBIG Fund or the borrower (1 original copy) - In case of discrepancy in name and other personal circumtances of owner/s or errors in technical description, registration of judicial correction or annotation of affidavit of correction.
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Security Documents</button>
                            <div class="panel py-2 bg-light">
                                        <div><b>a.</b> Deed of Absolute Sale executed by Developer in favor of the Pag-IBIG Fund or the borrower, as the case may be (HQP-HLF-236/237) (7 original copies).</div>

                                        <div><b>b.</b> Deed of Conditional Sale (HQP-HLF-234/235) between Pag-IBIG Fund and the borrower, if TCT/CCT is still under the name of Pag-IBIG Fund or Loan Mortgage Agreement if TCT/CCT is under the name of borrower (HQP-HLF-162/163) (7 original copies)</div>

                                        <div><b>c.</b> Duly Notarized Promissory Note (HQP-HLF-086/087 (7 original copies)</div>

                                        <div><b>d.</b> Signed Disclosure Statement on Loan Transaction with conformity of the borrower (HQP-HLF-085) (1 original copy and 2 photocopies)</div>

                                        <div><b>e.</b> Signed Notice of Approval with conformity of the borrower (HQP-HLF-152/153) (1 original copy and 2 photocopies)</div>
                                        <br>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Tax Declaration Land</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                        Updated Tax Declaration on the Land in the name of Pag-IBIG Fund or the borrower (1 photocopy)
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Tax Declaration</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                        Updated Tax Declaration on the improvements in the name of Pag-IBIG Fund or the borrower (1 photocopy) 
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Real Estate Tax Receipt</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                         Updated Real Estate Tax Receipt as the quarter immediately preceding the date of submission of documents in compliance to Notice of Approval (1 photocopy)
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Certificate of Acceptance</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                         Notarized Certificate of Acceptance (HQP-HLF-083) (1 photocopy)
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Occupancy Permit</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                         Occupancy Permit (1 photocopy)
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Transfer Tax Receipt</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                         Tranfer Tax Receipt for Lot and Building (1 photocopy)
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->

                            <!-- START -->
                            <div>
                            <button class="accordion"><b>+</b> &nbsp;Deduct Loan Amortization</button>
                            <div class="panel py-2 bg-light">
                                <ul>
                                    <li>
                                         Authority to Deduct Loan Amortization (HQP-HLF124/634) (1 original copy) for employers with existing CSA and in case that payment is through salary deduction.
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <!-- END -->
                            <!-- END PRIOR TO RELEASE OF LOAN PROCEEDS-->
                        <!-- END -->
                        </div>
                    </div>
                    <!-- START PAG-IBIG FUND -->
                </div>
            </section>
            <!-- END CHECKLIST OF REQUIRMENTS -->

            <!-- Start Ready to Apply -->
            <section class="bg-transparent shadow">
                <div class="container py-5">
                    <div class="row d-flex justify-content-center text-center">
                        <div class="col-lg-7 col-15 text-primary pt-5">
                            <h1>Ready to Apply?</h1>
                            <br>
                            <a href="application-form-ofw-ge.php" class="btn btn-outline-primary bg-primary rounded-pill px-md-5 px-4 py-2 radius-0 text-light light-300" style="background-color: #002d32; color: white">Click Here</a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Ready to Apply -->


<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>

<?php include ('assets/pages/footer.php') ?>