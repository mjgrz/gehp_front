<?php include ('assets/pages/header.php') ?>
<br><br><br><br>
    <!-- Start Aim -->
    <section class="banner-bg bg-white py-5 mb-5 pb-5">
        <div class="container my-4">
            <div class="row text-center">
                <div class="objective col-lg-6 mb-3">
                    <a href="application-form-ge.php" class="btn btn-outline-primary bg-secondary text-light px-2 py-md-4 py-1 radius-0" style="border-width: 2px;">
                        <h3 class="regular-400">Government Employee</h3>
                    </a>
                </div>
                <div class="objective col-lg-6 mb-3">
                    <a href="application-form-ofw.php" class="btn btn-outline-primary bg-secondary text-light px-2 py-md-4 py-1 radius-0" style="border-width: 2px;">
                        <h3 class="regular-400">Overseas Filipino Worker</h3>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Aim -->
<br><br><br>
<?php include ('assets/pages/footer.php') ?>