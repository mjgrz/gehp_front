<?php include ('assets/pages/header.php') ?> 
<script src="https://www.google.com/recaptcha/api.js"></script>                                


<!-- Start Contact -->
    <section class="container py-5">

        <h1 class="col-12 col-xl-8 h2 text-left text-primary pt-3">Application Form</h1>
        <p class="col-12 col-xl-8 text-left text-muted pb-5 typo-space-line">
            Government Employees/Uniformed Personnel Housing Program
        </p>

        <div class="row pb-4">
            <div class="col-lg-4">

                <div class="contact row mb-4 justify">
                    <div class="contact-icon col-lg-3 col-3">
                        <div class="py-3 mb-2 text-center border rounded text-secondary">
                            <i class='display-6 bx bxs-bell'></i>
                        </div>
                    </div>
                    <ul class="reminder-info list-unstyled col-lg-9 col-9">
                        <li class="h5 mb-0 mb-3">Reminder:</li>

                        <p>
                        <span class="light-300">
                           <i class="bx bxs-check-circle me-1"></i>Please read and follow the <b> Steps and Instructions</b>, as well as prepare all of the required documents listed on the <b> GEHP Requirements</b>. 
                        </span>
                        </p>

                        <p>
                        <span class="light-300">
                           <i class="bx bxs-check-circle me-1"></i>To avoid data entry errors, please ensure that all information you provide is complete and honest when filling out the form.
                        </span>
                        </p>

                        <p>
                        <span class="light-300">
                           <i class="bx bxs-check-circle me-1"></i>Kindly enter <b>'N/A'</b> if you don't have an answer to a specific field/s.
                        </span>
                        </p>

                        <p>
                        <span class="light-300">
                           Thank you!
                        </span>
                        </p>
                    </ul>
                </div>

                <div class="contact row mb-4 justify">
                    <div class="contact-icon col-lg-3 col-3">
                        <div class="py-3 mb-2 text-center border rounded text-secondary">
                            <i class='display-6 bx bxs-file'></i>
                        </div>
                    </div>
                    <ul class="reminder-info list-unstyled col-lg-9 col-9">
                        <li class="h5 mb-0 mb-3">Download Application Form</li>
                        <p>
                        <span class="light-300 justify">
                           <i class="bx bxs-check-circle me-1"></i> Download the PDF copy of GEHP Application Form and this shall be accomplished/filed by walk-in applicants. 
                        </span>
                        </p>
                        <p>
                        <a id="downloadpdf" href="print_AppGov.php">Click here to download</a>
                        <style>
                            #downloadpdf:hover {
                                color: #67A2B2;
                            }
                        </style>
                        </p>
                    </ul>
                </div>


            </div>


            <!-- Start Contact Form -->
            <div class="col-lg-8 ">
                <form class="contact-form row" method="post" action="submitGOV.php" role="form" enctype="multipart/form-data">
                    <!--action-->
                    <?php
                    include("../dbcon.php");
                    try{
                        $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $query = "SELECT * FROM applications";
                        $sthquery = $dbh->prepare($query);
                        $sthquery->execute();
                        $count = $sthquery->rowCount() + 1;
                        $dbh = "";
                    }
                    catch(PDOException $e){
                        error_log('PDOException - ' . $e->getMessage(), 0);
                        http_response_code(500);
                        die('Error establishing connection with database');
                    }
                    ?>
                    <input type="text" name="counter" value="<?php echo $count ?>" hidden>

                    <p class="h5 mb-4 typo-space-line"><b>A.1 FOR PURCHASE OF HOUSE AND LOT PACKAGE </b> <span class="text-muted"> under the GEHP thru</span></p>
                    <!-- Start Cash Sale  -->
                    <div class="col-lg-3 row mb-3">
                        <label class="container">
                          <input type="radio" name="payment" value="Cash Sale" required>
                          <span class="checkmark me-2"></span>
                          Cash Sale 
                        </label>
                    </div>
                    <!-- End Cash Sale  -->

                    <!-- Start Staggered Cash -->
                    <div class="col-lg-3 row mb-3">
                        <label class="container">
                          <input type="radio" name="payment" value="Staggered Cash" required>
                          <span class="checkmark me-2"></span>
                          Staggered Cash 
                        </label>
                    </div>
                    <!-- End Staggered Cash -->

                    <!-- Start End-User Financing -->
                    <div class="col-lg-5 row mb-3">
                        <label class="container">
                          <input type="radio" name="payment" value="End-User Financing (MTO-Pag-IBIG)" required>
                          <span class="checkmark me-2"></span>
                          End-User Financing (MTO-Pag-IBIG)
                        </label>
                    </div>
                    <!-- End End-User Financing -->

                    <div class="col-lg-12 row">
                        <div class="col-lg-5 row mb-3" style="width: auto;">
                            <label><b>In-House Financing (Installment Payment):</b></label>
                        </div> 
                        <!-- Start In-House Financing -->
                        <div class="col-lg-4 row mb-3" style="width: auto;">
                            <label class="container">
                            <input type="radio" name="payment" value="Straight Amortization" required>
                            <span class="checkmark me-2"></span>
                            Straight Amortization
                            </label>
                        </div>
                        <!-- In-House Financing -->
                        
                        <!-- Start In-House Financing -->
                        <div class="col-lg-4 row mb-3" style="width: auto;">
                            <label class="container">
                            <input type="radio" name="payment" value="Escalating Amortization" required>
                            <span class="checkmark me-2"></span>
                            Escalating Amortization
                            </label>
                        </div>
                        <!-- In-House Financing -->
                    </div>
                    

                    <!-- Start Straight Amortization  >
                    <div class="col-lg-6 row mb-3">
                        <label class="container">
                          <input type="radio" name="payment" value="Straight Amortization" required>
                          <span class="checkmark me-2"></span>
                          In-House Financing (Straight Amortization) 
                        </label>
                    </div>
                    < Straight Amortization -->

                    <!-- Start Escalating Amortization >
                    <div class="col-lg-6 row mb-3">
                        <label class="container">
                          <input type="radio" name="payment" value="Escalating Amortization" required>
                          <span class="checkmark me-2"></span>
                          In-House Financing (Escalating Amortization)
                        </label>
                    </div>
                    < Escalating Amortization  -->
		
		    <p class="pl-3">
			<b>A.1 </b> Reservation Fee of Php 20, 000, if applicable, is not refundable in case the applicant will not push through with the purchase.
		   </p><br>


                    <div class="col-lg-12 mb-4">
                        <div class="form-floating">
                            <select type="select" class="form-control btn-outline-primary bg-transparent text-dark" id="project" onchange="getProject()"
                                name="project" placeholder="Housing Project" required style="font-size: 14px;">
                            <option value="" disabled selected>Project Name</option>
                            <?php
                                include("../dbcon.php");
                                try{
                                  $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
                                  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                    $regionselect = "SELECT project_info.PJ_CODE, region.Region, project_info.PJ_NAME, units_floor.location FROM project_info 
                                                    LEFT JOIN units_floor ON project_info.project_id = units_floor.project_id
                                                    LEFT JOIN region ON project_info.region = region.psgc_reg
                                                    WHERE archived != '1' AND availability = 'Available' AND project_status != 'Upcoming'";
                                    $sthregionselect = $dbh->prepare($regionselect);
                                    $sthregionselect->execute();
                                    $sthregionselect->setFetchMode(PDO::FETCH_ASSOC); 
                                    if($sthregionselect->rowCount() > 0){
                                        while ($regionrow = $sthregionselect->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                    <option value="<?php echo $regionrow['PJ_CODE'] ?>"><?php echo $regionrow['Region'] ?> | <?php echo $regionrow['PJ_NAME'] ?> (<?php echo $regionrow['location'] ?>)</option>
                                        <?php 
                                        } 
                                        $dbh = "";
                                    }
                                    else { }
                                    }
                                catch(PDOException $e){
                                  error_log('PDOException - ' . $e->getMessage(), 0);
                                  http_response_code(500);
                                  die('Error establishing connection with database');
                                }
                                ?>
                            </select>
                            <label for="floatingname light-300">Select</label>
                        </div>
                        <div><span id="project-status"></span></div>
                        <script type="text/javascript">
                            function getProject(val){
                                $("#loaderIcon").show();
                                jQuery.ajax({
                                    url: "ajaxProject.php",
                                    data:'project='+$("#project").val(),
                                    type: "POST",
                                    success:function(data){
                                        $("#project-status").html(data);
                                        $("#loaderIcon").hide();
                                        },
                                    error:function (){
                                    }
                                });
                            }
                        </script>
                    </div><!-- End Select Project Name -->
                    <!-- End Input Project Name -->
                    <p class="h5 mb-4 typo-space-line"><b>A.2 TYPE OF HOUSING</b></p>
                    <div class="col-lg-3 row mb-4">
                        <label class="container">
                          <input type="radio" name="type" value="2-Storey Duplex">
                          <span class="checkmark me-2"></span>
                          2 Storey Duplex
                        </label>
                    </div>
                    <!-- End 2 Storey Duplex -->

                    <div class="col-lg-3 row mb-4">
                        <label class="container">
                          <input type="radio" name="type" value="1-Storey Duplex">
                          <span class="checkmark me-2"></span>
                          1 Storey Duplex
                        </label>
                    </div>
                    <!-- End 1 Storey Duplex -->

                    <div class="col-lg-5 row mb-4">
                        <label class="container">
                          <input type="radio" name="type" value="Low-Rise Building/Condominium">
                          <span class="checkmark me-2"></span>
                          Low-Rise Building/Condominium
                        </label>
                    </div>
                    <!-- End Low-Rise Building/Condominium -->

                    <p class="h5 mb-4 typo-space-line"><b>A.3 As Government Employee of</b></p>
                    <div class="col-lg-12 mb-5   ">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent regular-400 text-dark" id="employee" name="employee"
                            placeholder="Name of Government Agency" required>
                            <label for="floatingname light-300">Name of Government Agency</label>
                        </div>
                    </div><!-- End Input Last Name -->


                    <p class="h5 mb-5 typo-space-line"><b>I. APPLICANT'S IDENTITY</b> <span class="h6 text-muted">(For female applicant/spouse, provide a complete maiden name)</span></p>

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="lname" name="lname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Last Name" required>
                            <label for="floatingname light-300">Last Name</label>
                        </div>
                    </div><!-- End Input Last Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="fname" name="fname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="First Name" required>
                            <label for="floatingname light-300">First Name</label>
                        </div>
                    </div><!-- End Input First Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="mname" name="mname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Middle Name" required>
                            <label for="floatingname light-300">Middle Name</label>
                        </div>
                    </div><!-- End Input Middle Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="msurname" name="msurname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Mother's Surname" required>
                            <label for="floatingname light-300">Mother's Surname</label>
                        </div>
                    </div><!-- End Input Mother's Surname -->

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="address" name="address"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Residence Address" required>
                            <label for="floatingemail light-300">Residence Address</label>
                        </div>
                    </div><!-- End Input Residence Address -->
                    
                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="dplace" name="dplace"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Place of Birth" required>
                            <label for="floatingname light-300">Place of Birth</label>
                        </div>
                    </div><!-- End Place of Birth -->

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="email" name="email"
                            placeholder="Residence Address" required>
                            <label for="floatingphone light-300">Email address</label>
                        </div>
                    </div><!-- End Input Email address -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="date" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="dbirth" name="dbirth" placeholder="Date of Birth"
                            min="<?php echo Date('Y-m-d', strtotime('-65 year')); ?>" max="<?php echo date('Y-m-d', strtotime('-18 year')); ?>" required>
                            <label for="floatingname light-300">Date of Birth</label>
                        </div>
                    </div><!-- End Date of Birth -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <select type="select" class="form-control btn-outline-primary bg-transparent text-dark" id="sex" name="sex" placeholder="Select Sex" required>
                              <option value="" disabled selected>Sex</option>
                              <option value="male">Male</option>
                              <option value="female">Female</option>
                            </select>
                            <label for="floatingname light-300">Select</label>
                        </div>
                    </div><!-- End Sex -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <select type="select" class="form-control btn-outline-primary bg-transparent text-dark" id="status" name="status" placeholder="Select Civil Status" required>
                              <option value="" disabled selected>Civil Status</option>
                              <option value="single">Single</option>
                              <option value="married">Married</option>
                              <option value="widowed">Widowed</option>
                              <option value="separated-in-fact">Separated-in-Fact</option>
                              <option value="legally-separated">Legally-Separated</option>
                              <option value="single-HOF">Single-HOF</option>
                            </select>
                            <label for="floatingname light-300">Select</label>
                        </div>
                    </div><!-- End Select Civil Status -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="citizen" name="citizen"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Citizenship" required>
                            <label for="floatingname light-300">Citizenship</label>
                        </div>
                    </div><!-- End Place of Birth -->

                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="contact" name="contact" pattern="[0-9]+"
                            oninput="this.value = this.value.replace(/[^0-9-]/g, '').replace(/(\..*)\./g, '$1');" maxlength="11" placeholder="Contact Number (Residence)" required>
                            <label for="floatingemail light-300">Contact Number (Residence)</label>
                        </div>
                    </div><!-- End Contact Number (Residence) -->

                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="office" name="office" pattern="[0-9]+"
                            pattern="[0-9]+" oninput="this.value = this.value.replace(/[^0-9-]/g, '').replace(/(\..*)\./g, '$1');" maxlength="11" placeholder="Office" />
                            <label for="floatingphone light-300">Office</label>
                        </div>
                    </div><!-- End Office -->

                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="tin" name="tin"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="TIN No." required>
                            <label for="floatingphone light-300">TIN No.</label>
                        </div>
                    </div><!-- End TIN -->

                    <div class="col-lg-6 mb-4">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="pagibig" name="pagibig"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Pag-IBIG No." required>
                            <label for="floatingphone light-300">GSIS/SSS/Pag-IBIG Policy No.</label>
                        </div>
                    </div><!-- End Pag-IBIG No. -->


                    <h6><b>Applicant's Spouse / Co-Owner Identity:</b></h6>

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="s_lname" name="s_lname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Last Name">
                            <label for="floatingname light-300">Last Name</label>
                        </div>
                    </div><!-- End Input Spouse Last Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="s_fname" name="s_fname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="First Name">
                            <label for="floatingname light-300">First Name</label>
                        </div>
                    </div><!-- End Input Spouse First Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="s_mname" name="s_mname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Middle Name">
                            <label for="floatingname light-300">Middle Name</label>
                        </div>
                    </div><!-- End Input Spouse Middle Name -->

                    <div class="col-lg-3 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="s_msurname" name="s_msurname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Mother's Surname">
                            <label for="floatingname light-300">Mother's Surname</label>
                        </div>
                    </div><!-- End Input Spouse Mother's Surname -->

                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <input type="date" class="form-control btn-outline-primary bg-transparent text-dark light-300" id="s_dbirth" name="s_dbirth" placeholder="Date of Birth" min="<?php //echo Date('Y-m-d', strtotime('-65 year')); ?>" max="<?php echo date('Y-m-d', strtotime('-18 year')); ?>">
                            <label for="floatingname light-300">Date of Birth</label>
                        </div>
                    </div><!-- End Spouse Date of Birth -->

                    <div class="col-lg-6 mb-5">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="s_dplace" name="s_dplace"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Place of Birth">
                            <label for="floatingname light-300">Place of Birth</label>
                        </div>
                    </div><!-- End Spouse Place of Birth -->

                    <p class="h5 mb-5 typo-space-line"><b>II. APPLICANT'S EMPLOYMENT STATUS</b></p>

                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="nature" name="nature"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Nature of Employment" required>
                            <label for="floatingname light-300">Nature of Employment</label>
                        </div>
                    </div><!-- End Nature of Employment -->

                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="agency" name="agency"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Name of Agency/Corporation" required>
                            <label for="floatingname light-300">Name of Agency/Corporation</label>
                        </div>
                    </div><!-- End Name of Agency/Corporation -->

                    <div class="col-lg-12 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="a_address" name="a_address"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Name of Agency/Corporation" required>
                            <label for="floatingname light-300">Address of Agency/Corporation</label>
                        </div>
                    </div><!-- End Address of Agency/Corporation -->

                    <div class="col-lg-12 mb-3">
                        <div class="form-floating">
                            <input type="checkbox" class="" id="chck" name="chck"
                            onclick="disableName()">
                            <span for="light-300">Military Uniformed Personnel</span>
                        </div>
                    </div><!-- End Address of Agency/Corporation -->

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <select type="select" class="form-control text-dark input" id="BoS" name="BoS" disabled required >
                            <option value="">Branch of Service</option>
                            <option value="Air Force">Air Force</option>
                            <option value="Army">Army</option>
                            <option value="Navy">Navy</option>
                            </select>
                            <label id="bosLabel" for="floatingname light-300">Select (For AFP)</label> <span id="username-availability-status"></span>
                        </div>

                        <script type="text/javascript">
                            function getBranch()
                            {
                                jQuery.ajax({
                                type:'POST',
                                url:'ajax_branch.php',
                                data:'BoS='+$("#BoS").val(),
                                success:function(data){
                                    $("#username-availability-status").html(data);
                                }
                                })
                            }
                            
                        </script>
                    </div><!-- End Select Branch of Service -->

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control text-dark regular-400 input" id="rank" name="rank" disabled required
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Rank (For Uniformed Personnel)">
                            <label for="floatingname light-300">Rank <span class="text-muted" style="font-size: 14px;">(e.g Captain, Lieutenant)</span></label>
                        </div>
                    </div><!-- End Rank -->

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control text-dark regular-400 input" id="serial" name="serial" disabled required
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Serial" >
                            <label for="floatingname light-300">Serial No. / Badge No.</label>
                        </div>
                    </div><!-- End Serial No. -->
                    
                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control text-dark regular-400 input" id="assignPlace" name="assignPlace" disabled required
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="assignPlace" >
                            <label for="floatingname light-300">Place of Assignment</label>
                        </div>
                    </div><!-- End Serial No. -->

                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <select type="select" class="form-control text-dark input" id="motherUnit" name="motherUnit" disabled required >
                            <option value="">Mother Unit</option>
                            <option value="AFP">Armed Forces of the Philippines</option>
                            <option value="PNP">Philippine National Police</option>
                            <option value="BJMP">Bureau of Jail Management and Penology</option>
                            <option value="BFP">Bureau of Fire Protection</option>
                            <option value="BuCor">Bureau of Corrections</option>
                            <option value="PCG">Philippine Coast Guard</option>
                            </select>
                            <label for="floatingname light-300">Select (For Uniformed Personnel)</label>
                        </div>
                    </div><!-- End Position -->

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="position" name="position"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Position" required>
                            <label for="floatingname light-300">Position</label>
                        </div>
                    </div><!-- End Position -->

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <select type="select" class="form-control btn-outline-primary bg-transparent text-dark" id="SG" name="SG" placeholder="Select Civil Status" required>
                            <option value="">Salary Grade</option>
                            <option value="SG 1">Salary Grade 1</option>
                            <option value="SG 2">Salary Grade 2</option>
                            <option value="SG 3">Salary Grade 3</option>
                            <option value="SG 4">Salary Grade 4</option>
                            <option value="SG 5">Salary Grade 5</option>
                            <option value="SG 6">Salary Grade 6</option>
                            <option value="SG 7">Salary Grade 7</option>
                            <option value="SG 8">Salary Grade 8</option>
                            <option value="SG 9">Salary Grade 9</option>
                            <option value="SG 10">Salary Grade 10</option>
                            <option value="SG 11">Salary Grade 11</option>
                            <option value="SG 12">Salary Grade 12</option>
                            <option value="SG 13">Salary Grade 13</option>
                            <option value="SG 14">Salary Grade 14</option>
                            <option value="SG 15">Salary Grade 15</option>
                            <option value="SG 16">Salary Grade 16</option>
                            <option value="SG 17">Salary Grade 17</option>
                            <option value="SG 18">Salary Grade 18</option>
                            <option value="SG 19">Salary Grade 19</option>
                            <option value="SG 20">Salary Grade 20</option>
                            <option value="SG 21">Salary Grade 21</option>
                            <option value="SG 22">Salary Grade 22</option>
                            <option value="SG 23">Salary Grade 23</option>
                            <option value="SG 24">Salary Grade 24</option>
                            <option value="SG 25">Salary Grade 25</option>
                            <option value="SG 26">Salary Grade 26</option>
                            <option value="SG 27">Salary Grade 27</option>
                            <option value="SG 28">Salary Grade 28</option>
                            <option value="SG 29">Salary Grade 29</option>
                            <option value="SG 30">Salary Grade 30</option>
                            <option value="SG 31">Salary Grade 31</option>
                            <option value="SG 32">Salary Grade 32</option>
                            <option value="SG 33">Salary Grade 33</option>
                            </select>
                            <label for="floatingname light-300">Select</label>
                        </div>
                    </div><!-- End Salary Grade-->

                    <div class="col-lg-4 mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400"
                            oninput="this.value = this.value.replace(/[^0-9 ₱$€£¥ .,]/g, '').replace(/(\..*)\./g, '$1');"
                            maxlength="15" id="income" name="income" placeholder="Income" required>
                            <label for="floatingname light-300">Gross Monthly Income</label>
                        </div>
                    </div><!-- End Gross Monthly Income -->

                    <div class="col-lg-6 mb-3">
                        <div class="form-floating">
                            <input type="date" class="form-control btn-outline-primary bg-transparent text-dark regular-400 light-300" id="dHired" name="dHired" placeholder="Date Hired" min="<?php //echo Date('Y-m-d', strtotime('-2 year')); ?>" max="<?php echo date('Y-m-d'); ?>" required>
                            <label for="floatingname light-300">Date Hired</label>
                        </div>
                    </div><!-- End Date Hired -->

                    <div class="col-lg-6 mb-5">
                        <div class="form-floating">
                            <input type="number" class="form-control btn-outline-primary bg-transparent text-dark regular-400 light-300" max="60" min="0" id="years" name="years" placeholder="No. of years In the Government" required>
                            <label for="floatingname light-300">No. of years In the Government</label>
                        </div>
                    </div><!-- End No. of years In the Government -->

                    <p class="h5 mb-5 typo-space-line"><b>III. APPLICANT'S FAMILY COMPOSITION</b> <span class="text-muted h6 regular-400">(Add field/s if neccessary)</span></p>

                    <div class="multi-field-wrapper">
                        <div class="multi-fields">
                            <div class="col-lg-12 row multi-field" style="width: auto;">
                                <div class="col-lg-4 mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="sl form-control btn-outline-primary bg-transparent text-dark regular-400" name="slno[]" id="slno"  placeholder="Last Name" hidden readonly=""/>
                                        <input type="text" class="sl form-control btn-outline-primary bg-transparent text-dark regular-400" id="fam-lname" name="dlname[]"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Last Name"/>
                                        <label for="floatingname light-300">Last Name</label>
                                    </div>
                                </div><!-- End Input Last Name -->

                                <div class="col-lg-4 mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-300" id="fam-fname" name="dfname[]"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="First Name">
                                        <label for="floatingname text-dark regular-400">First Name</label>
                                    </div>
                                </div><!-- End Input First Name -->

                                <div class="col-lg-4 mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="fam-fname" name="dmname[]"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Middle Name">
                                        <label for="floatingname light-300">Middle Name</label>
                                    </div>
                                </div><!-- End Input Middle Name -->

                                <div class="col-lg-3 mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="fam-mname" name="relation[]"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Relation to Applicant">
                                        <label for="floatingname light-300" style="font-size: 15px; line-height: 16px;">Relation to Applicant</label>
                                    </div>
                                </div><!-- End Input Relation to Applicant -->

                                <div class="col-lg-3 mb-3">
                                    <div class="form-floating">
                                        <select type="select" class="form-control btn-outline-primary bg-transparent text-dark" id="dcstatus" name="dcstatus[]" placeholder="Select Civil Status">
                                            <option value="" disabled selected>Civil Status</option>
                                            <option value="single">Single</option>
                                            <option value="married">Married</option>
                                            <option value="widowed">Widowed</option>
                                            <option value="separated-in-fact">Separated-in-Fact</option>
                                            <option value="legally-separated">Legally-Separated</option>
                                            <option value="single-HOF">Single-HOF</option>
                                        </select>
                                        <label for="floatingname light-300">Select</label>
                                    </div>
                                </div><!-- End Input Civil Status -->

                                <div class="col-lg-3 mb-3">
                                    <div class="form-floating">
                                        <input type="number" class="form-control btn-outline-primary bg-transparent text-dark regular-400" min="0" max="150" id="fam-age" name="age[]" placeholder="Age">
                                        <label for="floatingname light-300">Age</label>
                                    </div>
                                </div><!-- End Input Age -->

                                <div class="col-lg-3 mb-3">
                                    <div class="form-floating">
                                        <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="fam-age" name="source_income[]"
                                        oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'.,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Age">
                                        <label for="floatingname light-300" style="font-size: 15px;">Source of Income</label>
                                    </div>
                                </div><!-- End Input Source of Income -->
                                <div class="col-lg-12 mb-3"> <hr> </div>
                            </div>
                        </div>
                        <div id="next"></div>
                        <button type="button" id="addrow" class="add-field btn btn-outline-primary btn-primary text-light regular-400 shadow-none mb-5"><i class='bx bx-plus'></i> Add member </button>
                    </div>

                    
                    <p style="float: left;" class="h5 mb-5 typo-space-line"><b>IV. APPLICANT'S TOTAL FAMILY INCOME PER MONTH:</b></p>
                    <div class="col-lg-6 mb-5">
                        <div class="form-floating">
                            <input type="text" pattern="[0-9]+" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');" maxlength="15" class="form-control btn-outline-primary bg-transparent text-dark regular-400" id="famIncome" name="famIncome" placeholder="famIncome" required>
                            <label style="font-weight: normal;" for="floatingname light-300">Total Family Income</label>
                        </div>
                    </div>

                    <p style="float: left;" class="h5 mb-4 typo-space-line">
                    <b>V. FAMILY REAL PROPERTY HOLDINGS:</b> 
                    
                    </p>
                    <span class="text-muted h6 regular-400">Have never availed of any form of government housing assistance</span>
                    <div class="col-lg-2 mb-5">
                        <label class="container">
                          <input type="radio" name="noViolation" value="YES" required>
                          <span class="checkmark me-2"></span>
                          YES
                        </label>
                    </div>
                    <div class="col-lg-2 mb-5">
                        <label class="container">
                          <input type="radio" name="noViolation" value="NO" required>
                          <span class="checkmark me-2"></span>
                          NO
                        </label>
                    </div>
                    <span class="text-muted h6 regular-400">Have sold, alienated, conveyed, encumbered or leased the socialized housing, including imporovements
                              or rights thereon, except to qualified beneficiary as determined by the Government Agency</span>
                    <div class="col-lg-2 mb-5">
                        <label class="container">
                          <input type="radio" name="altViolation" value="YES" required>
                          <span class="checkmark me-2"></span>
                          YES
                        </label>
                    </div>
                    <div class="col-lg-2 mb-5">
                        <label class="container">
                          <input type="radio" name="altViolation" value="NO" required>
                          <span class="checkmark me-2"></span>
                          NO
                        </label>
                    </div>

                    <div class="mb-3">
                    <p class="h6 text-primary">
                        <i class='bx bx-shield-quarter'></i>
                        By clicking Submit, you agree to our <a href="terms-of-service.php" class="text-secondary">Terms of Service</a> and that you have read our <a href="privacy-policy.php" class="text-secondary">Privacy Policy</a>.
                    </p>
                    </div><!-- End Privacy Policy -->

                    <div class="mb-3" style="float: right;">
                        <div class="g-recaptcha brochure__form__captcha" data-sitekey="6LdtNr8bAAAAAPx-lyX2nnrC-eri0pgtA357HQfA"></div>
                    </div><!-- End reCAPTCHA -->

                    <div class="col-md-12 col-12 m-auto text-end">
                        <button type="submit" id="submit_btn" class="btn btn-outline-primary bg-primary rounded-pill px-md-5 px-4 py-2 radius-0 text-light regular-400">Submit</button>
                    </div>

                </form>
            </div>
            <!-- End Contact Form -->


        </div>
    </section>
    <!-- End Contact -->
    <?php include ('assets/pages/footer.php') ?>
    
    <style>
        .input{
            border:1px solid #002d32;
        }
        .input:disabled{
            background-color:#e9ecef;
        }
        .input:hover{
            border-color: #67a2b2;   
        }
        .input:focus{
            -webkit-box-shadow: 0 0 0 0.25rem rgba(255, 255, 255, 1);
                    box-shadow: 0 0 0 0.25rem rgba(255, 255, 255, 1);
        }
        .input:active{
            -webkit-box-shadow: 0 0 0 0.25rem rgba(103, 162, 178, 1);
                    box-shadow: 0 0 0 0.25rem rgba(103, 162, 178, 1);
        }
        .input:disabled:active{
            -webkit-box-shadow: 0 0 0 0.25rem rgba(255, 255, 255, 1);
                    box-shadow: 0 0 0 0.25rem rgba(255, 255, 255, 1);
        }
    </style>
    <script>
        function disableName() {
            if(document.getElementById("chck").checked){
                document.getElementById("BoS").disabled = false;
                document.getElementById("rank").disabled = false;
                document.getElementById("serial").disabled = false;
                document.getElementById("assignPlace").disabled = false;
                document.getElementById("motherUnit").disabled = false;
            }
            else{
                document.getElementById("BoS").disabled = true;
                document.getElementById("rank").disabled = true;
                document.getElementById("serial").disabled = true;
                document.getElementById("assignPlace").disabled = true;
                document.getElementById("motherUnit").disabled = true;
                document.getElementById("BoS").value = "";
                document.getElementById("rank").value = "";
                document.getElementById("serial").value = "";
                document.getElementById("assignPlace").value = "";
                document.getElementById("motherUnit").value = "";
            }
        }
    </script>