<?php include ('assets/pages/header.php') ?>
<br><br><br><br>
    <!-- Start Aim -->
    <section class="banner-bg bg-white py-5 mb-5 pb-5">
        <div class="container my-4">
            <div class="row text-center">
                <div class="objective col-lg-6 mb-3">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSf0DVnszAMZG1_6morhwyFiij2CYb1dNV2p_SFEMkTSZs4mTQ/viewform?fbclid=IwAR0nAABLW7YiTY30ZouaGC5m_OrwxmMP15JOPjNXXnFwXhVb_aQhpWuiTb0" class="btn btn-outline-primary bg-secondary text-light px-2 py-md-4 py-1 radius-0" style="border-width: 2px;">
                        <h3 class="regular-400">Government Employee Survey</h3>
                    </a>
                </div>
                <div class="objective col-lg-6 mb-3">
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSfI5IW6-2WN7zKvriaQkta0seHeoV0WesLjrT21ULyb7s4ZNw/viewform" class="btn btn-outline-primary bg-secondary text-light px-2 py-md-4 py-1 radius-0" style="border-width: 2px;">
                        <h3 class="regular-400">Overseas Filipino Worker Survey</h3>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Aim -->
<br><br><br>
<?php include ('assets/pages/footer.php') ?>