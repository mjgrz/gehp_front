<?php
$id = $_GET["id"];
?>

<?php include ('assets/pages/header.php') ?>

<?php
//Bulletin
include("../dbcon.php");
try{
    $dbh = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $bulletinselect = "SELECT * FROM bulletin WHERE bulletin_id = :id";
    $sthbulletinselect = $dbh->prepare($bulletinselect);
    $sthbulletinselect->bindParam(':id', $id);
    $sthbulletinselect->execute();
    $sthbulletinselect->setFetchMode(PDO::FETCH_ASSOC); 
    while ($bulletinrow = $sthbulletinselect->fetch(PDO::FETCH_ASSOC)) {
        $bulletin_id = $bulletinrow["bulletin_id"];
        $bulletin_cat = $bulletinrow["bulletin_category"];
        $bulletin_title = $bulletinrow["bulletin_title"];
        $bulletin_desc = $bulletinrow["bulletin_desc"];
        $bulletin_img = $bulletinrow["bulletin_image"];
        $bulletin_date = date('F d, Y', strtotime($bulletinrow["date_upload"]));
    }
    $dbh = "";
}
catch(PDOException $e){
error_log('PDOException - ' . $e->getMessage(), 0);
http_response_code(500);
die('Error establishing connection with database');
}
?>
<!-- Start Progress -->
    <section class="bg-white py-5">
        <div class="container my-4">
            <div>
            <h1 class="creative-heading h2 pb-3 typo-space-line col-lg-8"><?php echo strtoupper($bulletin_title) ?></h1>
            <label class="text-muted light-300 py-3"><?php echo $bulletin_date ?></label>
            </div>

            <div class="creative-content row py-3">
                <div class="col-lg-7">
                    <p class="text-muted justify regular-400" style="text-align:justify;">
                        <?php echo nl2br($bulletin_desc); ?>
                    </p>
                </div>
                <div class="col-lg-5">
                    <div>
                        <a data-type="image" data-fslightbox="gallery" href="gehpbackend/pages/forms/uploads/bulletin/<?php echo $bulletin_img ?>">
                            <img class="img-fluid py-3" src="gehpbackend/pages/forms/uploads/bulletin/<?php echo $bulletin_img ?>">
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>
<!-- End Progress -->
    <div>
    <!-- Lightbox -->
        <script src="assets/js/fslightbox.js"></script>
        <script>
        fsLightboxInstances['gallery'].props.loadOnlyCurrentSource = true;
        </script>
        <?php include ('assets/pages/footer.php') ?>
    </div>