<?php include ('assets/pages/header.php') ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>                                    


<!-- Start Contact -->
    <section class="container py-5">

        <h1 class="col-12 col-xl-8 h2 text-left text-primary pt-3">Have any question?</h1>
        <p class="col-12 col-xl-8 text-left text-muted pb-5 light-300">
            Your opinions are important to us. Whether it is a simple question or a valuable suggestion, we are here 24/7.
        </p>

        <div class="row pb-4">
            <div class="col-lg-4">

                <div class="contact row mb-4">
                    <div class="contact-icon col-lg-3 col-3">
                        <div class="py-3 mb-2 text-center border rounded text-secondary">
                            <i class='display-6 bx bxs-phone'></i>
                        </div>
                    </div>
                    <ul class="contact-info list-unstyled col-lg-9 col-9">
                        <li class="h5 mb-0 mb-3">Office Contact</li>
                        <span>
                            <label class=""> Trunkline: </label> <label class="light-300">(02) 8790-0800 loc. 381</label>
                            <br>
                            <label class=""> Direct Line: </label> <label class="light-300"> 02-8922-2490 </label> 
                        </span>
                    </ul>
                </div>

                <div class="contact row mb-4">
                    <div class="contact-icon col-lg-3 col-3">
                        <div class="border py-3 mb-2 text-center border rounded text-secondary">
                            <i class='bx bx-shield-quarter display-6' ></i>
                        </div>
                    </div>
                    <ul class="contact-info list-unstyled col-lg-9 col-9">
                        <li class="h5 mb-0 mb-3">Info Privacy</li>
                        <span class="light-300">
                           Sharing your personal information with us is a top priority for us to secure; information will be protected from unauthorized access or misuse, and information confidentiality will be guaranteed.
                        </span>
                    </ul>                
                </div>
            </div>


            <!-- Start Contact Form -->
            <div class="col-lg-8 ">
                <form class="contact-form row" method="post" action="" role="form">

                    <div class="col-lg-4 mb-4">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-300" id="floatingname" name="fname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="First Name" required>
                            <label for="floatingname light-300">First Name</label>
                        </div>
                    </div><!-- End Input Name -->

                    <div class="col-lg-4 mb-4">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-300" id="floatingname" name="mname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Middle Name" required>
                            <label for="floatingname light-300">Middle Name</label>
                        </div>
                    </div><!-- End Input Name -->

                    <div class="col-lg-4 mb-4">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-300" id="floatingname" name="lname"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Last Name" required>
                            <label for="floatingname light-300">Last Name</label>
                        </div>
                    </div><!-- End Input Name -->

                    <div class="col-lg-6 mb-4">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-300" id="floatingemail" 
                            pattern="[^\s][A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z0-9]{2,4}$"
                            title="Valid and Active Email Account"
                            name="email" placeholder="Email" required>
                            <label for="floatingemail light-300">Email</label>
                        </div>
                    </div><!-- End Input Email -->

                    <div class="col-lg-6 mb-4">
                        <div class="form-floating">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-300" id="floatingphone" name="phone"
                            oninput="this.value = this.value.replace(/[^0-9-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Phone" required>
                            <label for="floatingphone light-300">Phone</label>
                        </div>
                    </div><!-- End Input Phone -->

                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control btn-outline-primary bg-transparent text-dark regular-300" id="floatingsubject"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" name="subject" placeholder="Subject" required>
                            <label for="floatingsubject light-300">Subject</label>
                        </div>
                    </div><!-- End Input Subject -->

                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <textarea class="form-control btn-outline-primary bg-transparent text-dark regular-300" style="resize: none; height: auto;" rows="8"
                            oninput="this.value = this.value.replace(/[^0-9 a-z A-Z'./,-]/g, '').replace(/(\..*)\./g, '$1');" placeholder="Message" name="message" id="message"
                            required></textarea>
                            <label for="floatingtextarea light-300">Message</label>
                        </div>
                    </div><!-- End Textarea Message -->

                    <div class="mb-3">
                    <p class="h6 text-primary">
                        <i class='bx bx-shield-quarter'></i>
                        By clicking Submit, you agree to our <a href="terms-of-service.php" class="text-secondary">Terms of Service</a> and that you have read our <a href="privacy-policy.php" class="text-secondary">Privacy Policy</a>.
                    </p>
                    </div><!-- End Privacy Policy -->

                    <div class="" style="float: right;">
                        <div class="g-recaptcha brochure__form__captcha" data-sitekey="6LcSEdgbAAAAAMth8l0bra8wXBnQzPSHu3o3MpEo"></div>
                    </div><!-- End reCAPTCHA -->

                    <div class="form-row pt-2">
                        <div class="col-md-12 col-10 text-end">
                            <button type="submit" 
                            class="btn btn-outline-primary bg-primary rounded-pill text-light px-md-4 px-2 py-md-2 py-1 radius-0 light-300">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- End Contact Form -->


        </div>
    </section>
    <!-- End Contact -->
    <?php include ('assets/pages/footer.php') ?>