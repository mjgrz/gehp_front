    <!-- Start Footer -->
    <footer class="bg-secondary pt-4">
        <div class="container">
            <div class="row py-4">

                <div class="col-lg-3 col-12 align-left">
                    <a class="navbar-brand" href="index.php">
                        <img src="assets/img/nha-logo.png" style="width: 50px" class="mb-6">
                        <img src="assets/img/logo_balai.png" style="width: 70px" class="mb-1">
                        <img src="assets/img/gehp_logo.png" style="width: 50px">
                        <br>
                    </a>
                    <p class="text-light my-lg-2 my-5 h6">
                        NHA for Government Employee's Housing Program
                    </p>
                    <ul class="list-inline footer-icons light-300">
                        <li class="list-inline-item m-0 py-2">
                            <a class="text-light" target="_blank" href="https://www.facebook.com/GovernmentEmployeesHousingProgram">
                                <i class='bx bxl-facebook-square bx-md'></i>
                            </a>
                        </li>
                        <li class="list-inline-item m-0">
                            <a class="text-light" target="_blank" href="https://www.linkedin.com/">
                                <i class='bx bxl-mail-square bx-md'></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-4 my-sm-0 mt-2">
                    <h3 class="h4 pb-lg-3 text-light">Our Company</h2>
                        <ul class="list-unstyled text-light">
                            <li class="pb-2">
                                <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light" href="index.php">Home</a>
                            </li>
                            <li class="pb-2">
                                <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="ongoing-projects.php">Projects</a>
                            </li>
                            <li class="pb-2">
                                <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="how-to-apply.php">How to Apply</a>
                            </li>
                            <li class="pb-2">
                                <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="about.php">About</a>
                            </li>
                            <li class="pb-2">
                                <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="contact.php">Contact Us</a>
                            </li>
                            <li class="pb-2">
                                <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="terms-of-service.php">Terms of Service</a>
                            </li>
                        </ul>
                </div>

                
                <div class="col-lg-3 col-md-4 my-sm-0 mt-4">
                    <h2 class="h4 pb-lg-3 text-light">For Clients</h2>
                    <ul class="list-unstyled text-light">
                        <li class="pb-2">
                            <i class='bx-fw bx bx-phone bx-xs'></i><a class="text-decoration-none text-light py-1" href="tel:(02) 8790-0800">Trunkline: (02) 8790-0800 loc. 381
                            <br><i class='bx-fw bx bx bx-xs'></i><a class="text-decoration-none text-light py-1" href="tel:02-8922-2490">Direct Line: 02-8922-2490
                        </li>
                        <li class="pb-2">
                            <i class='bx-fw bx bx-mail-send bx-xs'></i><a class="text-decoration-none text-light py-1" href="mailto:info@company.com">afppnp.npc@nha.gov.ph</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-4 my-sm-0 mt-4">
                    <h2 class="h4 pb-lg-3 text-light">Apply Now</h2>
                    <ul class="list-unstyled text-light">
                        <li class="pb-2">
                            <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="application-form-ge.php">GEHP Application Form</a>
                        </li>
                        <li class="pb-2">
                            <i class='bx-fw bx bxs-chevron-right bx-xs'></i><a class="text-decoration-none text-light py-1" href="application-form-ofw.php">OFW Application Form</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row d-flex"> </div> <!-- REMOVE LATER -->

        <div class="w-100 bg-primary py-3">
            <div class="container">
                <div class="row pt-2">
                    <div class="col-lg-6 col-sm-12">
                        <p class="text-lg-start text-center text-light">
                            © Copyright <?php echo date('Y');?> NHA | Government Employees' Housing Program
                        </p>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <p class="text-lg-end text-center text-light">
                            All Rights Reserved. <a rel="sponsored" class="text-decoration-none text-light" href="https://templatemo.com/" target="_blank"><strong> </strong></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->


    <!-- Bootstrap -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- Load jQuery require for isotope -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Isotope -->
    <script src="assets/js/isotope.pkgd.js"></script>
    <!-- Page Script -->
    <script type="text/javascript">
        //<![CDATA[
        $(function() {
            $('.multi-field-wrapper').each(function() {
                var $wrapper = $('.multi-fields', this);
                $(".add-field", $(this)).click(function(e) {
                    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('#fam-lname').val('').focus();
                });
                $('.multi-field .remove-field', $wrapper).click(function() {
                    if ($('.multi-field', $wrapper).length > 1)
                        $(this).parent('.multi-field').remove();
                });
            });
        }); //]]>
    </script>
</body>

</html>