<!-- Start Banner Hero -->
    <div class="banner-wrapper bg-dark">
        <div id="index_banner" class="banner-vertical-center-index container-fluid pt-5" style="background-image: url('assets/img/banner-abt.jpg');">

            <div style="height: 100%;">
                <div class="container">
            <div class="row d-flex align-items-center py-5">
                <div class="col-lg-6 text-start">
                    <h1 class="h2 py-5 text-primary typo-space-line">About Us</h1>
                    <h6 class="text-muted light-300">
                        Get to know more about National Housing Authority Government Employees' Housing Program
                    </h6>
                </div>
            </div>
        </div>
            </div>
        </div>
    </div>
    <!-- End Banner Hero -->