<!-- Start Banner Hero -->

<?php
//banner project
include("../dbcon.php");
$banner1id = 1;
if ( is_numeric($banner1id) == true){
    try{
        $dbhbanner = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbhbanner->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $bannerselect = "SELECT * FROM banner WHERE id = :id";
        $sthbanner = $dbhbanner->prepare($bannerselect);
        $sthbanner->bindParam(':id', $banner1id);
        $sthbanner->execute();
        $sthbanner->setFetchMode(PDO::FETCH_ASSOC);
        while ($bannerrow1 = $sthbanner->fetch(PDO::FETCH_ASSOC))  { 
            $banner_title1 = $bannerrow1["banner_title"];
            $banner_desc1 = $bannerrow1["banner_desc"];
            $banner_image1 = $bannerrow1["banner_image"];
        }
        $dbhbanner = null;
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
}
else{
http_response_code(400);
die('Error processing bad or malformed request');
}
?>

    <div class="banner-wrapper bg-dark">
        <div id="index_banner" class="banner-vertical-center-index container-fluid pt-5" style="background-image: url('../../gehpbackend/pages/forms/uploads/banner/<?php echo $banner_image1 ?>');">

            <div style="height: 100%;">
                <div class="container">
            <div class="row d-flex align-items-center py-5">
                <div class="col-lg-6 text-start">
                    <h1 class="h2 text-primary typo-space-line py-3"><?php echo $banner_title1 ?></h1>
                    <h6 class="text-muted light-300 py-3">
                    <?php echo $banner_desc1 ?>
                    </h6>
                </div>
            </div>
        </div>
            </div>
        </div>
    </div>
    <!-- End Banner Hero -->