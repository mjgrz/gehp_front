<!-- Start Banner Hero -->

<?php
//banner how to apply
include("../dbcon.php");
$banner2id = 2;
if ( is_numeric($banner2id) == true){
    try{
        $dbhbanner2 = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
        $dbhbanner2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $bannerselect2 = "SELECT * FROM banner WHERE id = :id";
        $sthbanner2 = $dbhbanner2->prepare($bannerselect2);
        $sthbanner2->bindParam(':id', $banner2id);
        $sthbanner2->execute();
        $sthbanner2->setFetchMode(PDO::FETCH_ASSOC);
        while ($bannerrow2 = $sthbanner2->fetch(PDO::FETCH_ASSOC))  { 
            $banner_title2 = $bannerrow2["banner_title"];
            $banner_desc2 = $bannerrow2["banner_desc"];
            $banner_image2 = $bannerrow2["banner_image"];
        }
        $dbhbanner2 = null;
    }
    catch(PDOException $e){
        error_log('PDOException - ' . $e->getMessage(), 0);
        http_response_code(500);
        die('Error establishing connection with database');
    }
}
else{
http_response_code(400);
die('Error processing bad or malformed request');
}
?>

    <div class="banner-wrapper bg-dark">
        <div id="index_banner" class="banner-vertical-center-index container-fluid pt-5" style="background-image: url('../../gehpbackend/pages/forms/uploads/banner/<?php echo $banner_image2 ?>');">

            <div style="height: 100%;">
                <div class="container">
            <div class="row d-flex align-items-center py-5">
                <div class="col-lg-6 text-start">
                    <h1 class="h2 py-5 text-primary typo-space-line"><?php echo $banner_title2 ?></h1>
                    <h6 class="text-muted light-300">
                    <?php echo $banner_desc2 ?>
                    </h6>
                </div>
            </div>
        </div>
            </div>
        </div>
    </div>
    <!-- End Banner Hero -->