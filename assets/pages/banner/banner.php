<!--Start Home Banner-->
<?php 
$slide1id = 1;
$slide2id = 1;
?>

<div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <?php
        //Slides
        if ( is_numeric($slide1id) == true){
          try{
            $dbhslide = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
            $dbhslide->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $slideselect = "SELECT * FROM homecarousel WHERE hc_id = :id";
            $sthslide = $dbhslide->prepare($slideselect);
            $sthslide->bindParam(':id', $slide1id);
            $sthslide->execute();
            $sthslide->setFetchMode(PDO::FETCH_ASSOC);
            while ($sliderow1 = $sthslide->fetch(PDO::FETCH_ASSOC))  { 
              $hc_image1 = $sliderow1["hc_image"];
        ?>
            <div class="carousel-item active">
              <img class="d-block w-100" src="../../gehpbackend/pages/forms/uploads/homeCarousel/<?php echo $hc_image1 ?>">
            </div>
        <?php
            }
            $dbhslide = null;
          }
          catch(PDOException $e){
              error_log('PDOException - ' . $e->getMessage(), 0);
              http_response_code(500);
              die('Error establishing connection with database');
          }
        }
        else{
        http_response_code(400);
        die('Error processing bad or malformed request');
        }
        ?>
        
        <?php
        if ( is_numeric($slide1id) == true){
          try{
            $dbhslide2 = new PDO('mysql:host='.$servername.';dbname='.$database.'', $username, $sLock);
            $dbhslide2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $slideselect2 = "SELECT * FROM homecarousel WHERE hc_id != :id";
            $sthslide2 = $dbhslide2->prepare($slideselect2);
            $sthslide2->bindParam(':id', $slide2id);
            $sthslide2->execute();
            $sthslide2->setFetchMode(PDO::FETCH_ASSOC);
            while ($sliderow2 = $sthslide2->fetch(PDO::FETCH_ASSOC))  { 
              $hc_image2 = $sliderow2["hc_image"];
        ?>
            <div class="carousel-item">
              <img class="d-block w-100" src="../../../gehpbackend/pages/forms/uploads/homeCarousel/<?php echo $hc_image2 ?>">
            </div>
        <?php
            }
            $dbhslide2 = null;
          }
          catch(PDOException $e){
              error_log('PDOException - ' . $e->getMessage(), 0);
              http_response_code(500);
              die('Error establishing connection with database');
          }
        }
        else{
        http_response_code(400);
        die('Error processing bad or malformed request');
        }
        ?>
        
        
    </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
      </a>
</div>

<!-- Page Script -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!--End Home Banner-->