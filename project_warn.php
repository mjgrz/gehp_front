<div style="color:red; width:80vh;" class="row">
    <div class="col-sm-1" style="padding:0px 0px 0px 6px;"><i class="bx bx-info-circle" style="width:100%; height:100%; font-size:50px;"></i></div>
    <div class="col-sm-11">
        <span>
            This project is currently not accepting new applications due to large number of registered applicants.
            </br>
            Sorry for the inconvenience.
        </span>
    </div>
</div>